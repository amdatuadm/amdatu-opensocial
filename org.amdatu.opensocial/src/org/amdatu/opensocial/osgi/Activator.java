/*
 * Copyright (c) 2010-2012 - The Amdatu Foundation.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.opensocial.osgi;

import java.util.Dictionary;
import java.util.Hashtable;

import javax.servlet.Servlet;

import org.amdatu.opensocial.Constants;
import org.amdatu.opensocial.gadget.Gadget;
import org.amdatu.opensocial.gadget.GadgetProvider;
import org.amdatu.opensocial.gadget.impl.GadgetProviderImpl;
import org.amdatu.opensocial.spi.ActivityBackendService;
import org.amdatu.opensocial.spi.ActivityStreamBackendService;
import org.amdatu.opensocial.spi.AlbumBackendService;
import org.amdatu.opensocial.spi.AppDataBackendService;
import org.amdatu.opensocial.spi.GroupBackendService;
import org.amdatu.opensocial.spi.MediaItemBackendService;
import org.amdatu.opensocial.spi.MessageBackendService;
import org.amdatu.opensocial.spi.PersonBackendService;
import org.amdatu.opensocial.spi.bridge.ActivityServiceBridge;
import org.amdatu.opensocial.spi.bridge.ActivityStreamServiceBridge;
import org.amdatu.opensocial.spi.bridge.AlbumServiceBridge;
import org.amdatu.opensocial.spi.bridge.AppDataServiceBridge;
import org.amdatu.opensocial.spi.bridge.GroupServiceBridge;
import org.amdatu.opensocial.spi.bridge.MediaItemServiceBridge;
import org.amdatu.opensocial.spi.bridge.MessageServiceBridge;
import org.amdatu.opensocial.spi.bridge.PersonServiceBridge;
import org.apache.felix.dm.DependencyActivatorBase;
import org.apache.felix.dm.DependencyManager;
import org.apache.shindig.auth.AuthenticationServletFilter;
import org.apache.shindig.gadgets.servlet.ConcatProxyServlet;
import org.apache.shindig.gadgets.servlet.GadgetRenderingServlet;
import org.apache.shindig.gadgets.servlet.HtmlAccelServlet;
import org.apache.shindig.gadgets.servlet.JsServlet;
import org.apache.shindig.gadgets.servlet.MakeRequestServlet;
import org.apache.shindig.gadgets.servlet.OAuth2CallbackServlet;
import org.apache.shindig.gadgets.servlet.OAuthCallbackServlet;
import org.apache.shindig.gadgets.servlet.ProxyServlet;
import org.apache.shindig.gadgets.servlet.RpcServlet;
import org.apache.shindig.gadgets.servlet.RpcSwfServlet;
import org.apache.shindig.protocol.DataServiceServlet;
import org.apache.shindig.protocol.JsonRpcServlet;
import org.apache.shindig.social.core.oauth2.OAuth2Servlet;
import org.apache.shindig.social.opensocial.spi.ActivityService;
import org.apache.shindig.social.opensocial.spi.ActivityStreamService;
import org.apache.shindig.social.opensocial.spi.AlbumService;
import org.apache.shindig.social.opensocial.spi.AppDataService;
import org.apache.shindig.social.opensocial.spi.GroupService;
import org.apache.shindig.social.opensocial.spi.MediaItemService;
import org.apache.shindig.social.opensocial.spi.MessageService;
import org.apache.shindig.social.opensocial.spi.PersonService;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleContext;
import org.osgi.service.http.HttpContext;
import org.osgi.service.log.LogService;

/**
 * Provides the bundle activator for registering the Shindig servlets/filters
 * in a whiteboard-style.
 */
public class Activator extends DependencyActivatorBase {

    private static final String SERVLET_ALIAS = "alias";

    @Override
    public void destroy(BundleContext context, DependencyManager manager) throws Exception {
        // Nop
    }

    @Override
    public void init(BundleContext context, DependencyManager manager) throws Exception {
        registerLogHandler(manager);

        registerGuiceInjectorProvider(manager);

        registerHttpContext(manager);

        registerAuthenticationFilter(manager);

        // Core data services...
        registerDataServiceServlet(manager);
        registerJsonRpcServlet(manager);

        // Gadget related services...
        registerGadgetRenderingServlet(manager);
        registerProxyServlet(manager);
        registerHtmlAccelServlet(manager);
        registerMakeRequestServlet(manager);
        registerConcatProxyServlet(manager);
        registerRpcServlet(manager);
        registerJsServlet(manager);
        registerRpcSwfServlet(manager);

        // Auth related services...
        registerOAuthCallbackServlet(manager);
        registerOAuth2CallbackServlet(manager);
        registerOAuth2Servlet(manager);
        
        // Register the gadget provider...
        manager.add(createComponent()
            .setInterface(GadgetProvider.class.getName(), null)
            .setImplementation(GadgetProviderImpl.class)
            .add(createServiceDependency()
                .setService(Gadget.class)
                .setCallbacks("addGadget", "removeGadget")
                .setRequired(false))
            );
        
        // Register all SPI service bridges...
        manager.add(createComponent()
            .setInterface(ActivityService.class.getName(), null)
            .setImplementation(ActivityServiceBridge.class)
            .add(createServiceDependency()
                .setService(ActivityBackendService.class)
                .setRequired(true))
            );
        manager.add(createComponent()
            .setInterface(ActivityStreamService.class.getName(), null)
            .setImplementation(ActivityStreamServiceBridge.class)
            .add(createServiceDependency()
                .setService(ActivityStreamBackendService.class)
                .setRequired(true))
            );
        manager.add(createComponent()
            .setInterface(AlbumService.class.getName(), null)
            .setImplementation(AlbumServiceBridge.class)
            .add(createServiceDependency()
                .setService(AlbumBackendService.class)
                .setRequired(true))
            );
        manager.add(createComponent()
            .setInterface(AppDataService.class.getName(), null)
            .setImplementation(AppDataServiceBridge.class)
            .add(createServiceDependency()
                .setService(AppDataBackendService.class)
                .setRequired(true))
            );
        manager.add(createComponent()
            .setInterface(GroupService.class.getName(), null)
            .setImplementation(GroupServiceBridge.class)
            .add(createServiceDependency()
                .setService(GroupBackendService.class)
                .setRequired(true))
            );
        manager.add(createComponent()
            .setInterface(MediaItemService.class.getName(), null)
            .setImplementation(MediaItemServiceBridge.class)
            .add(createServiceDependency()
                .setService(MediaItemBackendService.class)
                .setRequired(true))
            );
        manager.add(createComponent()
            .setInterface(MessageService.class.getName(), null)
            .setImplementation(MessageServiceBridge.class)
            .add(createServiceDependency()
                .setService(MessageBackendService.class)
                .setRequired(true))
            );
        manager.add(createComponent()
            .setInterface(PersonService.class.getName(), null)
            .setImplementation(PersonServiceBridge.class)
            .add(createServiceDependency()
                .setService(PersonBackendService.class)
                .setRequired(true))
            );
    }

    /**
     * Helper method for creating a dictionary instance with the given initial values as key/values.
     */
    private Dictionary<Object, Object> createServiceProps(Object... values) {
        if (values == null || values.length % 2 != 0) {
            throw new IllegalArgumentException("Invalid values!");
        }
        Hashtable<Object, Object> props = new Hashtable<Object, Object>();
        for (int i = 0; i < values.length; i += 2) {
            props.put(values[i], values[i + 1]);
        }
        // Mandatory property for all services registered in this class...
        props.put("contextId", Constants.CONTEXT_NAME);
        return props;
    }

    /**
     * Registers the authentication filter, used by all servlets for their authentication handling.
     */
    private void registerAuthenticationFilter(DependencyManager manager) {
        AuthenticationServletFilter filter = new AuthenticationServletFilter();
        
        Integer rank = Integer.valueOf(10);

        registerFilter(manager, filter, createServiceProps("pattern", "/social.*", "service.ranking", rank));
        registerFilter(manager, filter, createServiceProps("pattern", "/gadgets/ifr.*", "service.ranking", rank));
        registerFilter(manager, filter, createServiceProps("pattern", "/gadgets/makeRequest.*", "service.ranking", rank));
        registerFilter(manager, filter, createServiceProps("pattern", "/gadgets/proxy.*", "service.ranking", rank));
        registerFilter(manager, filter, createServiceProps("pattern", "/gadgets/api/rpc.*", "service.ranking", rank));
        registerFilter(manager, filter, createServiceProps("pattern", "/gadgets/api/rest.*", "service.ranking", rank));
        registerFilter(manager, filter, createServiceProps("pattern", "/rpc.*", "service.ranking", rank));
//        registerFilter(manager, filter, createServiceProps("pattern", "/rest.*", "service.ranking", rank));
    }

    /**
     * Registers a servlet for concatenating proxy requests?
     */
    private void registerConcatProxyServlet(DependencyManager manager) {
        registerServlet(manager, ConcatProxyServlet.class, createServiceProps(SERVLET_ALIAS, "/gadgets/concat"));
    }

    /**
     * Registers the servlets(!) for serving REST data-services.
     */
    private void registerDataServiceServlet(DependencyManager manager) {
    	// TODO in the web.xml this was one instance of the servlet
//        registerServlet(manager, DataServiceServlet.class, createServiceProps(SERVLET_ALIAS, "/rest", "init.handlers", "org.apache.shindig.handlers"));
        registerServlet(manager, DataServiceServlet.class, createServiceProps(SERVLET_ALIAS, "/gadgets/api/rest", "init.handlers", "org.apache.shindig.handlers"));
        registerServlet(manager, DataServiceServlet.class, createServiceProps(SERVLET_ALIAS, "/social/rest", "init.handlers", "org.apache.shindig.handlers"));
    }

    /**
     * Registers a given filter as OSGi service.
     */
    private void registerFilter(DependencyManager manager, javax.servlet.Filter filter, Dictionary<Object, Object> props) {
        manager.add(createComponent()
            .setInterface(javax.servlet.Filter.class.getName(), props)
            .setImplementation(new GuiceFilterWrapper(filter))
            .setAutoConfig(javax.servlet.Filter.class, false)
            .add(createServiceDependency()
                .setService(GuiceInjectorProvider.class)
                .setRequired(true)));
    }

    /**
     * Registers a servlet that can render a Gadget.
     */
    private void registerGadgetRenderingServlet(DependencyManager manager) {
        registerServlet(manager, GadgetRenderingServlet.class, createServiceProps(SERVLET_ALIAS, "/gadgets/ifr"));
    }

    /**
     * Registers a valid {@link GuiceInjectorProvider} for use with the servlets.
     */
    private void registerGuiceInjectorProvider(DependencyManager manager) {
        manager.add(createComponent()
            .setInterface(GuiceInjectorProvider.class.getName(), null)
            .setImplementation(ShindigGuiceService.class)
            .add(createConfigurationDependency()
                .setPid(Constants.SHINDIG_CONFIGURATION_PID)
            )
            .add(createServiceDependency()
                .setService(LogService.class)
                .setRequired(false))
            );
    }

    /**
     * Registers a servlet for HTML acceleration?
     */
    private void registerHtmlAccelServlet(DependencyManager manager) {
        registerServlet(manager, HtmlAccelServlet.class, createServiceProps(SERVLET_ALIAS, "/gadgets/accel"));
    }

    /**
     * Registers the custom HTTP context for all servlets.
     */
    private void registerHttpContext(DependencyManager manager) {
        manager.add(createComponent()
            .setInterface(HttpContext.class.getName(), createServiceProps())
            .setImplementation(ShindigHttpContext.class)
            .add(createBundleDependency()
                .setStateMask(Bundle.ACTIVE)
                .setFilter(String.format("(%s=*)", Constants.RESOURCE_PROVIDER_KEY))
                .setCallbacks("addResourceBundle", "removeResourceBundle"))
            .add(createServiceDependency()
                .setService(LogService.class)
                .setRequired(false))
        );
    }

    /**
     * Registers the servlets(!) for serving the social RPC API.
     */
    private void registerJsonRpcServlet(DependencyManager manager) {
    	// TODO in the web.xml this was one instance of the servlet
        registerServlet(manager, JsonRpcServlet.class, createServiceProps(SERVLET_ALIAS, "/rpc", "init.handlers", "org.apache.shindig.handlers"));
        registerServlet(manager, JsonRpcServlet.class, createServiceProps(SERVLET_ALIAS, "/gadgets/api/rpc", "init.handlers", "org.apache.shindig.handlers"));
        registerServlet(manager, JsonRpcServlet.class, createServiceProps(SERVLET_ALIAS, "/social/rpc", "init.handlers", "org.apache.shindig.handlers"));
    }

    /**
     * Registers the servlet for serving JavaScript.
     */
    private void registerJsServlet(DependencyManager manager) {
        registerServlet(manager, JsServlet.class, createServiceProps(SERVLET_ALIAS, "/gadgets/js"));
    }

    /**
     * Registers a custom loghandler that tries to redirect JUL to the LogService of OSGi.
     */
    private void registerLogHandler(DependencyManager manager) {
        manager.add(createComponent()
            .setImplementation(LogHandler.class)
            .add(createServiceDependency()
                .setService(LogService.class)
                .setRequired(false)));
    }

    /**
     * Registers a servlet for making requests to other gadgets?
     */
    private void registerMakeRequestServlet(DependencyManager manager) {
        registerServlet(manager, MakeRequestServlet.class, createServiceProps(SERVLET_ALIAS, "/gadgets/makeRequest"));
    }

    /**
     * Registers the callback servlet for OAuth2.
     */
    private void registerOAuth2CallbackServlet(DependencyManager manager) {
        registerServlet(manager, OAuth2CallbackServlet.class, createServiceProps(SERVLET_ALIAS, "/gadgets/oauth2callback"));
    }

    /**
     * Registers the servlet for serving OAuth 2 APIs.
     */
    private void registerOAuth2Servlet(DependencyManager manager) {
        registerServlet(manager, OAuth2Servlet.class, createServiceProps(SERVLET_ALIAS, "/oauth2"));
    }

    /**
     * Registers the callback servlet for OAuth.
     */
    private void registerOAuthCallbackServlet(DependencyManager manager) {
        registerServlet(manager, OAuthCallbackServlet.class, createServiceProps(SERVLET_ALIAS, "/gadgets/oauthcallback"));
    }

    /**
     * Registers a proxy servlet.
     */
    private void registerProxyServlet(DependencyManager manager) {
        registerServlet(manager, ProxyServlet.class, createServiceProps(SERVLET_ALIAS, "/gadgets/proxy"));
    }

    /**
     * Registers the servlet for handling metadata RPC.
     */
    private void registerRpcServlet(DependencyManager manager) {
        registerServlet(manager, RpcServlet.class, createServiceProps(SERVLET_ALIAS, "/gadgets/metadata"));
    }

    /**
     * Registers the servlet for handling RPC through SWF?
     */
    private void registerRpcSwfServlet(DependencyManager manager) {
        registerServlet(manager, RpcSwfServlet.class, createServiceProps(SERVLET_ALIAS, "/xpc"));
    }

    /**
     * Registers a given servlet as OSGi service.
     */
    private void registerServlet(DependencyManager manager, Class servlet, Dictionary<Object, Object> props) {
        manager.add(createComponent()
            .setInterface(Servlet.class.getName(), props)
            .setImplementation(new GuiceServletWrapper(servlet))
            .setAutoConfig(Servlet.class, false)
            .add(createServiceDependency()
                .setService(GuiceInjectorProvider.class)
                .setRequired(true)));
    }
}
