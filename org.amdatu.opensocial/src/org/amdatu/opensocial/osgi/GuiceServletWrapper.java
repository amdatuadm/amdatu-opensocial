/*
 * Copyright (c) 2010-2012 - The Amdatu Foundation.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.opensocial.osgi;

import java.io.IOException;

import javax.servlet.Servlet;
import javax.servlet.ServletConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;


import com.google.inject.Injector;

/**
 * Provides a wrapper for all servlets that allows Guice's {@link Injector} to 
 * be used in the {@link #init(ServletConfig)} method.
 */
public class GuiceServletWrapper implements Servlet {
    
    static final String ATTR_GUICE_INJECTOR = "guice-injector";

    private final Class m_delegateClass;
    private Servlet m_delegate;
    // Injected by Felix DM...
    private volatile GuiceInjectorProvider m_injectorProvider;
    
    /**
     * Creates a new {@link GuiceServletWrapper} for the given servlet.
     * 
     * @param delegate the servlet to delegate all calls to.
     */
    public GuiceServletWrapper(Class delegateClass) {
        m_delegateClass = delegateClass;
    }

    @Override
    public void destroy() {
        m_delegate.destroy();
    }

    @Override
    public ServletConfig getServletConfig() {
        return m_delegate.getServletConfig();
    }

    @Override
    public String getServletInfo() {
        return m_delegate.getServletInfo();
    }

    @Override
    public void init(ServletConfig config) throws ServletException {
        ServletContext context = config.getServletContext();
        context.setAttribute(ATTR_GUICE_INJECTOR, m_injectorProvider.getInjector());
        try {
        	m_delegate = (Servlet) m_delegateClass.newInstance();
            m_delegate.init(config);
        }
        catch (Exception e) {
        	throw new ServletException("Could not initialize servlet because delegate could not be created.", e);
		}
        finally {
            context.removeAttribute(ATTR_GUICE_INJECTOR);
        }
    }

    @Override
    public void service(ServletRequest request, ServletResponse response) throws ServletException, IOException {
        m_delegate.service(request, response);
    }
}
