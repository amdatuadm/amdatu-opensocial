/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package org.amdatu.opensocial.osgi;

import java.util.ArrayList;
import java.util.List;

import org.apache.shindig.auth.AuthenticationHandler;
import org.apache.shindig.auth.AuthenticationHandlerProvider;

/**
 * Default implementation for {@link AuthenticationHandlerProvider} that can
 * be used in an OSGi environment.
 * 
 * <p>
 * TODO: ensure the proper ordering of the handlers!
 * </p>
 */
public class OsgiAuthenticationHandlerProvider implements
		AuthenticationHandlerProvider {
	private final List<AuthenticationHandler> m_handlers = new ArrayList<AuthenticationHandler>();

	@Override
	public List<AuthenticationHandler> getHandlers() {
		return m_handlers;
	}

	public void addHandler(AuthenticationHandler handler) {
		m_handlers.add(handler);
	}

	public void removeHandler(AuthenticationHandler handler) {
		m_handlers.remove(handler);
	}
}