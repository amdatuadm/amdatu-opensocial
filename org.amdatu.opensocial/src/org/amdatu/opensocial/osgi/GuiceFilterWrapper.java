/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package org.amdatu.opensocial.osgi;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;

import com.google.inject.Injector;

/**
 * Provides a wrapper for all filter that allows Guice's {@link Injector} to be
 * used in the {@link #init(FilterConfig)} method.
 */
public class GuiceFilterWrapper implements Filter {

	static final String ATTR_GUICE_INJECTOR = GuiceServletWrapper.ATTR_GUICE_INJECTOR;

	private final Filter m_delegate;
	// Injected by Felix DM...
	private volatile GuiceInjectorProvider m_injectorProvider;

	/**
	 * Creates a new {@link GuiceFilterWrapper} instance for the given filter
	 * delegate.
	 * 
	 * @param delegate
	 *            the original filter to delegate all calls to, cannot be
	 *            <code>null</code>.
	 */
	public GuiceFilterWrapper(Filter delegate) {
		m_delegate = delegate;
	}

	@Override
	public void destroy() {
		m_delegate.destroy();
	}

	@Override
	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		m_delegate.doFilter(request, response, chain);
	}

	@Override
	public void init(FilterConfig config) throws ServletException {
		ServletContext context = config.getServletContext();

		context.setAttribute(ATTR_GUICE_INJECTOR, m_injectorProvider.getInjector());
		try {
			m_delegate.init(config);
		} finally {
            context.removeAttribute(ATTR_GUICE_INJECTOR);
		}
	}
}
