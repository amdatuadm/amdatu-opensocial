/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package org.amdatu.opensocial.osgi;

import java.net.URL;
import java.util.Comparator;
import java.util.Scanner;
import java.util.Set;
import java.util.concurrent.ConcurrentMap;
import java.util.concurrent.ConcurrentSkipListMap;
import java.util.concurrent.CopyOnWriteArraySet;

import org.amdatu.opensocial.Constants;
import org.osgi.framework.Bundle;

/**
 * Provides a registry for resource to bundle mappings.
 * <p>
 * This registry can register bundles having a {@link Constants#RESOURCE_PROVIDER_KEY} entry in their manifest. The
 * value of this header should be a comma-separated list of paths to resources the bundle provides. Optionally each
 * path can provide an additional prefix to map external paths to internal paths.<br/>
 * For example, consider the following value: <tt>/foo;prefix=resources</tt>. This means that all resources starting
 * with <tt>/foo</tt> will be mapped internally to <tt>/resources/foo</tt>. Without the <tt>prefix=</tt> option, the
 * mapping would simply be <tt>/foo</tt>.
 * </p>
 */
final class ResourceRegistry {

    /**
     * Comparator that sorts on string lengths in descending order (longest first), in case of equal lengths, the order is determined by the natural sort order of the string contents.
     */
    static class StringLengthComparator implements Comparator<String> {
        @Override
        public int compare(String o1, String o2) {
            int r = o2.length() - o1.length();
            if (r == 0) {
                r = o2.compareTo(o1);
            }
            return r;
        }
    }

    /**
     * Provides a small container for keeping a resource prefix and a bundle together.
     */
    static class BundleMapping {
        private final String m_prefix;
        private final Bundle m_bundle;

        private BundleMapping(String prefix, Bundle bundle) {
            m_prefix = prefix;
            m_bundle = bundle;
        }

        public static BundleMapping create(String key, Bundle bundle) {
            String prefix = "";
            int idx = key.indexOf(';');
            if ((idx >= 0) && (idx < (key.length() - 1))) {
                key = key.substring(idx + 1);
                if (key.startsWith("prefix=")) {
                    prefix = key.substring(7).trim();
                }
            }

            return new BundleMapping(prefix, bundle);
        }

        @Override
        public boolean equals(Object object) {
            if (this == object) {
                return true;
            }
            if (object == null || object.getClass() != getClass()) {
                return false;
            }

            BundleMapping that = (BundleMapping) object;
            return m_prefix.equals(that.m_prefix) && m_bundle.equals(that.m_bundle);
        }

        public URL getResource(String name) {
            if (!"".equals(m_prefix.trim())) {
                name = m_prefix.concat("/").concat(name);
            }
            return m_bundle.getResource(name);
        }

        @Override
        public int hashCode() {
            return m_prefix.hashCode() ^ m_bundle.hashCode();
        }
    }

    private final ConcurrentMap<String, Set<BundleMapping>> m_resources;

    /**
     * Creates a new {@link ResourceRegistry} instance.
     */
    public ResourceRegistry() {
        m_resources = new ConcurrentSkipListMap<String, Set<BundleMapping>>(new StringLengthComparator());
    }

    /**
     * Finds the URL for the given name by scanning the registry for entries
     * starting with the same common prefix as the given resource.
     * 
     * @param resourceName the resource to retrieve, cannot be <code>null</code>.
     * @return an URL pointing to the requested resource, or <code>null</code>
     *         if the requested resource could not be found.
     */
    public URL getResource(String resourceName) {
        if (resourceName.startsWith("/")) {
            resourceName = resourceName.substring(1);
        }

        // Walk across all keys sorted by their length (longest first). By
        // doing this, we can avoid false positives for paths with common
        // prefixes (such as /resources/specific/path and /resources)...
        for (String key : m_resources.keySet()) {
            if (resourceName.startsWith(key)) {
                Set<BundleMapping> bundleMappings = m_resources.get(key);
                if (bundleMappings != null) {
                    for (BundleMapping mapping : bundleMappings) {
                        URL result = mapping.getResource(resourceName);
                        if (result != null) {
                            return result;
                        }
                    }
                }
            }
        }

        return null;
    }

    /**
     * Registers the given bundle as being a resource provider. It should
     * contain a bundle header names {@link Constants#RESOURCE_PROVIDER_KEY} that provides the directory names (comma separated) containing the
     * resources.
     * 
     * @param bundle the bundle to register, cannot be <code>null</code>.
     */
    public void registerResources(Bundle bundle) {
        String bundleHeader = (String) bundle.getHeaders().get(Constants.RESOURCE_PROVIDER_KEY);
        if ((bundleHeader != null) && !"".equals(bundleHeader.trim())) {
            Scanner scanner = new Scanner(bundleHeader);
            scanner.useDelimiter(",\\s*");

            while (scanner.hasNext()) {
                String entry = scanner.next();
                String resourceKey = sanitize(entry);

                Set<BundleMapping> bundles = m_resources.get(resourceKey);
                if (bundles == null) {
                    bundles = new CopyOnWriteArraySet<BundleMapping>();

                    Set<BundleMapping> oldValue = m_resources.putIfAbsent(resourceKey, bundles);
                    if (oldValue != null) {
                        // Lost the race condition: use the list returned instead...
                        bundles = oldValue;
                    }
                }

                bundles.add(BundleMapping.create(entry, bundle));
            }
        }
    }

    /**
     * Unregisters a given bundle as being a resource provider.
     * 
     * @param bundle the bundle to unregister, cannot be <code>null</code>.
     */
    public void unregisterResources(Bundle bundle) {
        String key = (String) bundle.getHeaders().get(Constants.RESOURCE_PROVIDER_KEY);
        if ((key != null) && !"".equals(key.trim())) {
            Scanner scanner = new Scanner(key);
            scanner.useDelimiter(",\\s*");

            while (scanner.hasNext()) {
                String entry = scanner.next();
                String resourceKey = sanitize(entry);

                Set<BundleMapping> bundles = m_resources.get(resourceKey);
                if (bundles != null) {
                    bundles.remove(BundleMapping.create(entry, bundle));

                    if (bundles.isEmpty()) {
                        // Remove entry altogether...
                        m_resources.remove(key, bundles);
                    }
                }
            }
        }
    }

    /**
     * Sanitizes the given input string for use in this registry.
     * 
     * @param input the input to sanitize, cannot be <code>null</code>.
     * @return the sanitized input, that is, stripped of the (optional) leading slash and without any metadata.
     */
    private String sanitize(String input) {
        if (input.startsWith("/")) {
            input = input.substring(1);
        }
        int idx = input.indexOf(';');
        if (idx >= 0) {
            return input.substring(0, idx);
        }
        return input;
    }
}
