/*
 * Copyright (c) 2010-2012 - The Amdatu Foundation.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.opensocial.osgi;

import java.util.ArrayList;
import java.util.Dictionary;
import java.util.Enumeration;
import java.util.List;
import java.util.Properties;

import org.amdatu.opensocial.module.GadgetAdminModule;
import org.amdatu.opensocial.module.OAuth2Module;
import org.amdatu.opensocial.module.OAuthModule;
import org.amdatu.opensocial.module.SocialApiModule;
import org.apache.felix.dm.Component;
import org.apache.shindig.common.PropertiesModule;
import org.apache.shindig.common.cache.ehcache.EhCacheModule;
import org.apache.shindig.common.servlet.GuiceServletContextListener.CleanupHandler;
import org.apache.shindig.extras.ShindigExtrasGuiceModule;
import org.apache.shindig.gadgets.DefaultGuiceModule;
import org.ops4j.peaberry.Peaberry;
import org.osgi.framework.BundleContext;
import org.osgi.service.cm.ConfigurationException;
import org.osgi.service.cm.ManagedService;
import org.osgi.service.log.LogService;

import com.google.inject.Guice;
import com.google.inject.Injector;
import com.google.inject.Module;
import com.google.inject.Stage;

/**
 * Sets up Guice for use with Shindig.
 */
public class ShindigGuiceService implements GuiceInjectorProvider, ManagedService {

	// Injected by Felix DM...
	private volatile BundleContext m_context;
	private volatile LogService m_log;
	private volatile Injector m_injector;

	private Properties m_properties;

	/**
	 * Called by Felix DM.
	 */
	public void init(Component component) throws Exception {
		List<Module> modules = loadModules();

		try {
			m_injector = Guice.createInjector(Stage.PRODUCTION, modules);
		} catch (Exception e) {
			e.printStackTrace();
			m_log.log(LogService.LOG_WARNING, "Failed to create injector!", e);
		}

		m_log.log(LogService.LOG_INFO, "Shindig Guice service initialized ...");
	}

	/**
	 * Called by Felix DM.
	 */
	public void stop(Component component) throws Exception {
		if (m_injector != null) {
			CleanupHandler cleanups = m_injector.getInstance(CleanupHandler.class);
			cleanups.cleanup();
		}
		m_injector = null;
	}

	@Override
	public Injector getInjector() {
		return m_injector;
	}

	/**
	 * Loads all Guice module definitions known for Shindig.
	 * 
	 * @return a list of loaded Guice {@link Module}s.
	 */
	private List<Module> loadModules() {
		List<Module> modules = new ArrayList<Module>();
		
		modules.add(Peaberry.osgiModule(m_context));
		// Standard Shindig modules...
		modules.add(new PropertiesModule(m_properties) {
            /** Overridden to allow 'shindig.contextroot' to be defined by means of the generic properties. */
            @Override
		    protected String getContextRoot() {
                // this method is called before the properties are read; 
                // make sure to use our local properties object...
                if ((m_properties != null) && m_properties.containsKey("shindig.contextroot")) {
                    return m_properties.getProperty("shindig.contextroot");
                }
		        return super.getContextRoot();
		      }

            /** Overridden to allow 'shindig.port' to be defined by means of the generic properties. */
		    @Override
		    protected String getServerPort() {
		        // this method is called after the properties are read; 
		        // so we can safely use the reference from the module itself...
		        Properties props = getProperties();
		        if (props.containsKey("shindig.port")) {
		            return props.getProperty("shindig.port");
		        }
		        return super.getServerPort();
		    }
		    
		    /** Overridden to allow 'shindig.host' to be defined by means of the generic properties. */
		    @Override
		    protected String getServerHostname() {
                // this method is called after the properties are read; 
                // so we can safely use the reference from the module itself...
                Properties props = getProperties();
                if (props.containsKey("shindig.host")) {
                    return props.getProperty("shindig.host");
                }
		        return super.getServerHostname();
		    }
		});
		modules.add(new EhCacheModule());
		modules.add(new DefaultGuiceModule());
		modules.add(new ShindigExtrasGuiceModule());
		// Custom (Peaberry) modules...
		modules.add(new SocialApiModule());
		modules.add(new OAuthModule());
        modules.add(new OAuth2Module());
        modules.add(new GadgetAdminModule());

		return modules;
	}

	@SuppressWarnings("rawtypes")
	@Override
	public void updated(Dictionary properties) throws ConfigurationException {
		m_properties = new Properties();
		Enumeration keys = properties.keys();
		while (keys.hasMoreElements()) {
			Object key = keys.nextElement();
			m_properties.put(key, properties.get(key));
		}
	}
}
