/*
 * Copyright (c) 2010-2012 - The Amdatu Foundation.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.opensocial.osgi;

import java.io.IOException;
import java.net.URL;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.framework.Bundle;
import org.osgi.service.http.HttpContext;
import org.osgi.service.log.LogService;

/**
 * Provides a custom HTTP context for the Shindig OpenSocial implementation.
 */
public class ShindigHttpContext implements HttpContext {

    // Injected by Felix DM...
    private volatile LogService m_log;

    private final ResourceRegistry m_registry;

    /**
     * Creates a new {@link ShindigHttpContext} instance.
     */
    public ShindigHttpContext() {
        m_registry = new ResourceRegistry();
    }

    /**
     * Adds a given bundle as resource provider to this HTTP context.
     * 
     * @param bundle the bundle to add as resource provider, cannot be <code>null</code>.
     */
    public void addResourceBundle(Bundle bundle) {
        m_registry.registerResources(bundle);
    }

    @Override
    public boolean handleSecurity(HttpServletRequest request, HttpServletResponse response) throws IOException {
        // TODO implement properly...
        return true;
    }

    @Override
    public URL getResource(String name) {
        URL result = m_registry.getResource(name);

        if (result != null) {
            m_log.log(LogService.LOG_DEBUG, "Found resource: " + name);
        }
        else {
            m_log.log(LogService.LOG_DEBUG, "Resource: " + name + " not found!");
        }

        return result;
    }

    @Override
    public String getMimeType(String name) {
        // TODO Auto-generated method stub
        return null;
    }

    /**
     * Removes a given bundle as resource provider to this HTTP context.
     * 
     * @param bundle the bundle to remove as resource provider, cannot be <code>null</code>.
     */
    public void removeResourceBundle(Bundle bundle) {
        m_registry.unregisterResources(bundle);
    }
}
