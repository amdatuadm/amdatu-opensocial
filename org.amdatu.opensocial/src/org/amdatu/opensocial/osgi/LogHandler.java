package org.amdatu.opensocial.osgi;

import java.util.ArrayList;
import java.util.List;
import java.util.logging.Handler;
import java.util.logging.Level;
import java.util.logging.LogManager;
import java.util.logging.LogRecord;
import java.util.logging.Logger;

import org.osgi.service.log.LogService;

public class LogHandler extends Handler {
	private volatile LogService m_log;
	private final List<Handler> m_originalHandlers = new ArrayList<Handler>();

	public void start() {
    	Logger root = LogManager.getLogManager().getLogger("");
    	Handler[] hs = root.getHandlers();
    	for (Handler h : hs) {
    		root.removeHandler(h);
    		m_originalHandlers.add(h);
    	}
    	root.addHandler(this);
	}
	
	public void stop() {
    	Logger root = LogManager.getLogManager().getLogger("");
		root.removeHandler(this);
		for (Handler h : m_originalHandlers) {
			root.addHandler(h);
		}
		m_originalHandlers.clear();
	}
	
	@Override
	public void publish(LogRecord record) {
		m_log.log(mapLevel(record.getLevel()), record.getMessage(), record.getThrown());
	}

	/** Map the log levels of the Java logging API to those of the OSGi LogService. */
	private int mapLevel(Level level) {
		int value = level.intValue();
		if (value >= Level.SEVERE.intValue()) {
			return LogService.LOG_ERROR;
		}
		if (value >= Level.WARNING.intValue()) {
			return LogService.LOG_WARNING;
		}
		if (value >= Level.INFO.intValue()) {
			return LogService.LOG_INFO;
		}
		return LogService.LOG_DEBUG;
	}

	@Override
	public void flush() {
	}

	@Override
	public void close() throws SecurityException {
	}
}
