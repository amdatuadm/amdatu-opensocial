/*
 * Copyright (c) 2010-2012 - The Amdatu Foundation.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.opensocial;

import org.osgi.service.http.HttpContext;

/**
 * Provides the exported constants for this bundle.
 */
public interface Constants {

    /**
     * The <tt>contextId</tt> of the {@link HttpContext} to use.
     */
    String CONTEXT_NAME = "Amdatu-OpenSocial";

    /**
     * The bundle key to use to register additional resources from external bundles.
     */
    String RESOURCE_PROVIDER_KEY = "Amdatu-OpenSocial-ResourceProvider";

    /**
     * Configuration PID for the global Shindig configuration (shindig.properties).
     */
    String SHINDIG_CONFIGURATION_PID = "org.amdatu.opensocial.shindig";
}
