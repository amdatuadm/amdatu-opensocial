/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package org.amdatu.opensocial.spi;

import java.util.Date;
import java.util.Map;

/**
 * Provides the various options to query/filter on in backend services.
 */
public interface QueryOptions {

    /**
     * Returns which field of the object being filtered on should be filtered on.
     * <p>
     * This filter can be any field of the object being filtered or the special JS filters, hasApp or topFriends.
     * Other special Filters are
     * </p>
     * <dl>
     * <dt>all</dt>
     * <dd>Retrieves all friends;</dd>
     * <dt>hasApp</dt>
     * <dd>Retrieves all friends with any data for this application;</dd>
     * <dt>topFriends</dt>
     * <dd>Retrieves only the user's top friends;</dd>
     * <dt>isFriendsWith</dt>
     * <dd>Will filter the people requested by checking if they are friends with
     * the given idSpec. Expects a filterOptions parameter to be passed which defines the following field:
     * <tt>idSpec</tt> representing the idSpec that each person must be friends with.</dd>
     * </dl>
     * 
     * @return the name of the field to filter by, can be <code>null</code>.
     */
    String getFilter();

    /**
     * @return the filter operation, either one of "contains", "equals", "startsWith", or "present".
     */
    String getFilterOperation();

    /**
     * When a field filter has been specified (i.e. a non special filter) then this is the value of the
     * filter. The exception is the isFriendsWith filter where this contains the value of the id who
     * the all the results need to be friends with.
     * 
     * @return the filter value, can be <code>null</code>.
     */
    String getFilterValue();

    /**
     * When paginating, the index of the first item to fetch.
     * 
     * @return the value of first index to fetch, > 0.
     */
    int getFirst();

    /**
     * When paginating, the maximum number of items to fetch.
     * 
     * @return the number of items to fetch, > 0, defaults to 20.
     */
    int getMax();

    /**
     * @return the optionalParameters for the filter criteria, never <code>null</code>.
     */
    Map<String, String> getOptionalParameters();

    /**
     * @return the name of the field to sort on, can be <code>null</code>.
     */
    String getSortBy();

    /**
     * @return the sort order, can be either "ascending", or "descending".
     */
    String getSortOrder();

    /**
     * @return the date since when the data should be updated.
     */
    Date getUpdatedSince();

}
