/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package org.amdatu.opensocial.spi;

import java.util.Collection;

import org.apache.shindig.social.opensocial.model.Group;

/**
 * Provides a simple facade for <tt>org.apache.shindig.social.opensocial.spi.GroupService</tt>.
 */
public interface GroupBackendService {

    /**
     * Gathers group information for the specified user.
     * 
     * @param userId the ID of the user to retrieve the groups for;
     * @param fields the fields to query/search for;
     * @param options the query options to use.
     * @return a collection of groups, never <code>null</code>.
     * @throws BackendException in case of problems retrieving the groups.
     */
    Collection<Group> getGroups(String userId, Collection<String> fields, QueryOptions options) throws BackendException;
}
