/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package org.amdatu.opensocial.spi.bridge;

import java.util.HashSet;
import java.util.Set;

import org.amdatu.opensocial.spi.QueryOptions;
import org.apache.shindig.auth.SecurityToken;
import org.apache.shindig.social.opensocial.spi.CollectionOptions;
import org.apache.shindig.social.opensocial.spi.GroupId;
import org.apache.shindig.social.opensocial.spi.UserId;
import org.apache.shindig.social.opensocial.spi.GroupId.Type;

/**
 * Provides some common methods used by some of the bridge implementations.
 */
final class BridgeUtils {

    /**
     * Converts a given {@link GroupId} to a string representation.
     * 
     * @param groupId the group ID to convert, cannot be <code>null</code>.
     * @return the string representation of the group ID, never <code>null</code>.
     */
    public static String getGroupId(GroupId groupId) {
        Type type = groupId.getType();
        if (type == Type.objectId) {
            return groupId.toString(); // XXX is this correct?
        }
        return "@".concat(type.name());
    }
    
    public static String getUserId(UserId userId, SecurityToken token) {
        return userId.getUserId(token);
    }

    /**
     * Converts the given {@link CollectionOptions} into {@link QueryOptions}.
     * 
     * @param options the options to convert, cannot be <code>null</code>.
     * @return the converted options, never <code>null</code>.
     */
    public static QueryOptions getQueryOptions(CollectionOptions options) {
        return new QueryOptionsImpl(options);
    }

    public static Set<String> getUserList(Set<UserId> userIds, SecurityToken token) {
        Set<String> paramList = new HashSet<String>();
        for (UserId u : userIds) {
            try {
                String uid = u.getUserId(token);
                if (uid != null) {
                    paramList.add(uid);
                }
            }
            catch (IllegalStateException istate) {
                // ignore the user id.
            }
        }
        return paramList;
    }

    /**
     * Creates a new {@link BridgeUtils} instance, never used.
     */
    private BridgeUtils() {
        // Nop
    }

}
