/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package org.amdatu.opensocial.spi.bridge;

import static org.amdatu.opensocial.spi.bridge.BridgeUtils.getQueryOptions;
import static org.amdatu.opensocial.spi.bridge.BridgeUtils.getUserId;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Future;

import javax.servlet.http.HttpServletResponse;

import org.amdatu.opensocial.spi.BackendException;
import org.amdatu.opensocial.spi.GroupBackendService;
import org.amdatu.opensocial.spi.QueryOptions;
import org.apache.shindig.auth.SecurityToken;
import org.apache.shindig.common.util.ImmediateFuture;
import org.apache.shindig.protocol.ProtocolException;
import org.apache.shindig.protocol.RestfulCollection;
import org.apache.shindig.social.opensocial.model.Group;
import org.apache.shindig.social.opensocial.spi.CollectionOptions;
import org.apache.shindig.social.opensocial.spi.GroupService;
import org.apache.shindig.social.opensocial.spi.UserId;

/**
 * Bridges the {@link GroupService} SPI interface of Shindig to a less obtrusive API.
 */
public class GroupServiceBridge implements GroupService {
    
    // Injected by Felix DM...
    private volatile GroupBackendService m_backend;

    /**
     * Creates a new {@link GroupServiceBridge} instance.
     */
    public GroupServiceBridge() {
        // Nop
    }

    @Override
    public Future<RestfulCollection<Group>> getGroups(UserId userId, CollectionOptions options, Set<String> fields, SecurityToken token) throws ProtocolException {
        String _user = getUserId(userId, token);
        QueryOptions _options = getQueryOptions(options);
        
        try {
            List<Group> groups = new ArrayList<Group>(m_backend.getGroups(_user, fields, _options));

            return ImmediateFuture.newInstance(new RestfulCollection<Group>(groups));
        }
        catch (BackendException e) {
            throw new ProtocolException(HttpServletResponse.SC_BAD_REQUEST, e.getMessage(), e);
        }
    }
}
