/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package org.amdatu.opensocial.spi.bridge;

import static org.amdatu.opensocial.spi.bridge.BridgeUtils.getGroupId;
import static org.amdatu.opensocial.spi.bridge.BridgeUtils.getQueryOptions;
import static org.amdatu.opensocial.spi.bridge.BridgeUtils.getUserId;
import static org.amdatu.opensocial.spi.bridge.BridgeUtils.getUserList;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Future;

import javax.servlet.http.HttpServletResponse;

import org.amdatu.opensocial.spi.ActivityStreamBackendService;
import org.amdatu.opensocial.spi.BackendException;
import org.amdatu.opensocial.spi.QueryOptions;
import org.apache.shindig.auth.SecurityToken;
import org.apache.shindig.common.util.ImmediateFuture;
import org.apache.shindig.protocol.ProtocolException;
import org.apache.shindig.protocol.RestfulCollection;
import org.apache.shindig.social.opensocial.model.ActivityEntry;
import org.apache.shindig.social.opensocial.spi.ActivityStreamService;
import org.apache.shindig.social.opensocial.spi.CollectionOptions;
import org.apache.shindig.social.opensocial.spi.GroupId;
import org.apache.shindig.social.opensocial.spi.UserId;

/**
 * Bridges the {@link ActivityStreamService} SPI interface of Shindig to a less obtrusive API.
 */
public class ActivityStreamServiceBridge implements ActivityStreamService {

    // Injected by Felix DM...
    private volatile ActivityStreamBackendService m_backend;

    /**
     * Creates a new {@link ActivityStreamServiceBridge} instance.
     */
    public ActivityStreamServiceBridge() {
        // Nop
    }

    public Future<ActivityEntry> createActivityEntry(UserId userId, GroupId groupId, String appId,
        Set<String> fields, ActivityEntry activity, SecurityToken token) throws ProtocolException {
        String _userId = getUserId(userId, token);
        String _groupId = getGroupId(groupId);

        try {
            ActivityEntry entry = m_backend.createActivityEntry(_userId, _groupId, appId, fields, activity);

            return ImmediateFuture.newInstance(entry);
        }
        catch (BackendException e) {
            throw new ProtocolException(HttpServletResponse.SC_BAD_REQUEST, e.getMessage(), e);
        }
    }

    public Future<Void> deleteActivityEntries(UserId userId, GroupId groupId, String appId,
        Set<String> activityIds, SecurityToken token) throws ProtocolException {
        String _userId = getUserId(userId, token);
        String _groupId = getGroupId(groupId);

        try {
            m_backend.deleteActivityEntries(_userId, _groupId, appId, activityIds);

            return ImmediateFuture.newInstance(null);
        }
        catch (BackendException e) {
            throw new ProtocolException(HttpServletResponse.SC_BAD_REQUEST, e.getMessage(), e);
        }
    }

    public Future<RestfulCollection<ActivityEntry>> getActivityEntries(Set<UserId> userIds,
        GroupId groupId, String appId, Set<String> fields, CollectionOptions options, SecurityToken token)
        throws ProtocolException {
        Set<String> _userIds = getUserList(userIds, token);
        String _groupId = getGroupId(groupId);
        QueryOptions _options = getQueryOptions(options);

        try {
            List<ActivityEntry> activityEntries =
                new ArrayList<ActivityEntry>(m_backend.getActivityEntries(_userIds, _groupId, appId, fields, _options));

            return ImmediateFuture.newInstance(new RestfulCollection<ActivityEntry>(activityEntries));
        }
        catch (BackendException e) {
            throw new ProtocolException(HttpServletResponse.SC_BAD_REQUEST, e.getMessage(), e);
        }
    }

    public Future<RestfulCollection<ActivityEntry>> getActivityEntries(UserId userId, GroupId groupId,
        String appId, Set<String> fields, CollectionOptions options, Set<String> activityIds, SecurityToken token)
        throws ProtocolException {
        String _userId = getUserId(userId, token);
        String _groupId = getGroupId(groupId);
        QueryOptions _options = getQueryOptions(options);

        try {
            List<ActivityEntry> activityEntries =
                new ArrayList<ActivityEntry>(m_backend.getActivityEntries(_userId, _groupId, appId, fields, _options,
                    activityIds));

            return ImmediateFuture.newInstance(new RestfulCollection<ActivityEntry>(activityEntries));
        }
        catch (BackendException e) {
            throw new ProtocolException(HttpServletResponse.SC_BAD_REQUEST, e.getMessage(), e);
        }
    }

    public Future<ActivityEntry> getActivityEntry(UserId userId, GroupId groupId, String appId,
        Set<String> fields, String activityId, SecurityToken token)
        throws ProtocolException {
        String _userId = getUserId(userId, token);
        String _groupId = getGroupId(groupId);

        try {
            ActivityEntry activityEntry = m_backend.getActivityEntry(_userId, _groupId, appId, fields, activityId);

            return ImmediateFuture.newInstance(activityEntry);
        }
        catch (BackendException e) {
            throw new ProtocolException(HttpServletResponse.SC_BAD_REQUEST, e.getMessage(), e);
        }
    }

    public Future<ActivityEntry> updateActivityEntry(UserId userId, GroupId groupId, String appId,
        Set<String> fields, ActivityEntry activity, String activityId,
        SecurityToken token) throws ProtocolException {
        String _userId = getUserId(userId, token);
        String _groupId = getGroupId(groupId);

        try {
            ActivityEntry entry = m_backend.updateActivityEntry(_userId, _groupId, appId, fields, activity, activityId);

            return ImmediateFuture.newInstance(entry);
        }
        catch (BackendException e) {
            throw new ProtocolException(HttpServletResponse.SC_BAD_REQUEST, e.getMessage(), e);
        }
    }
}
