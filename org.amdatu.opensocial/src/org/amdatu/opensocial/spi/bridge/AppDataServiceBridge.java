/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package org.amdatu.opensocial.spi.bridge;

import static org.amdatu.opensocial.spi.bridge.BridgeUtils.*;

import java.util.Map;
import java.util.Set;
import java.util.concurrent.Future;

import javax.servlet.http.HttpServletResponse;

import org.amdatu.opensocial.spi.AppDataBackendService;
import org.amdatu.opensocial.spi.BackendException;
import org.apache.shindig.auth.SecurityToken;
import org.apache.shindig.common.util.ImmediateFuture;
import org.apache.shindig.protocol.DataCollection;
import org.apache.shindig.protocol.ProtocolException;
import org.apache.shindig.social.opensocial.spi.AppDataService;
import org.apache.shindig.social.opensocial.spi.GroupId;
import org.apache.shindig.social.opensocial.spi.UserId;

/**
 * Bridges the {@link AppDataService} SPI interface of Shindig to a less obtrusive API.
 */
public class AppDataServiceBridge implements AppDataService {

    // Injected by Felix DM...
    private volatile AppDataBackendService m_backend;

    /**
     * Creates a new {@link AppDataServiceBridge} instance.
     */
    public AppDataServiceBridge() {
        // Nop
    }

    public Future<Void> deletePersonData(UserId userId, GroupId groupId, String appId, Set<String> fields,
        SecurityToken token) throws ProtocolException {
        String _user = getUserId(userId, token);
        String _groupId = getGroupId(groupId);

        try {
            m_backend.deletePersonData(_user, _groupId, appId, fields);

            return ImmediateFuture.newInstance(null);
        }
        catch (BackendException e) {
            throw new ProtocolException(HttpServletResponse.SC_BAD_REQUEST, e.getMessage(), e);
        }
    }

    public Future<DataCollection> getPersonData(Set<UserId> userIds, GroupId groupId, String appId, Set<String> fields,
        SecurityToken token) throws ProtocolException {
        Set<String> _userIds = getUserList(userIds, token);
        String _groupId = getGroupId(groupId);

        try {
            DataCollection data = m_backend.getPersonData(_userIds, _groupId, appId, fields);

            return ImmediateFuture.newInstance(data);
        }
        catch (BackendException e) {
            throw new ProtocolException(HttpServletResponse.SC_BAD_REQUEST, e.getMessage(), e);
        }
    }

    public Future<Void> updatePersonData(UserId userId, GroupId groupId,
        String appId, Set<String> fields, Map<String, String> values, SecurityToken token)
        throws ProtocolException {
        String _user = getUserId(userId, token);
        String _groupId = getGroupId(groupId);

        try {
            m_backend.updatePersonData(_user, _groupId, appId, fields, values);

            return ImmediateFuture.newInstance(null);
        }
        catch (BackendException e) {
            throw new ProtocolException(HttpServletResponse.SC_BAD_REQUEST, e.getMessage(), e);
        }
    }
}
