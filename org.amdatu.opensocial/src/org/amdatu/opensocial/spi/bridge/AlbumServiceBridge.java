/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package org.amdatu.opensocial.spi.bridge;

import static org.amdatu.opensocial.spi.bridge.BridgeUtils.getGroupId;
import static org.amdatu.opensocial.spi.bridge.BridgeUtils.getQueryOptions;
import static org.amdatu.opensocial.spi.bridge.BridgeUtils.getUserId;
import static org.amdatu.opensocial.spi.bridge.BridgeUtils.getUserList;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Future;

import javax.servlet.http.HttpServletResponse;

import org.amdatu.opensocial.spi.AlbumBackendService;
import org.amdatu.opensocial.spi.BackendException;
import org.amdatu.opensocial.spi.QueryOptions;
import org.apache.shindig.auth.SecurityToken;
import org.apache.shindig.common.util.ImmediateFuture;
import org.apache.shindig.protocol.ProtocolException;
import org.apache.shindig.protocol.RestfulCollection;
import org.apache.shindig.social.opensocial.model.Album;
import org.apache.shindig.social.opensocial.spi.AlbumService;
import org.apache.shindig.social.opensocial.spi.CollectionOptions;
import org.apache.shindig.social.opensocial.spi.GroupId;
import org.apache.shindig.social.opensocial.spi.UserId;

/**
 * Bridges the {@link AlbumService} SPI interface of Shindig to a less obtrusive API.
 */
public class AlbumServiceBridge implements AlbumService {

    // Injected by Felix DM...
    private volatile AlbumBackendService m_backend;

    /**
     * Creates a new {@link AlbumServiceBridge} instance.
     */
    public AlbumServiceBridge() {
        // Nop
    }

    public Future<Void> createAlbum(UserId userId, String appId, Album album,
        SecurityToken token) throws ProtocolException {
        String _userId = getUserId(userId, token);

        try {
            m_backend.createAlbum(_userId, appId, album);

            return ImmediateFuture.newInstance(null);
        }
        catch (BackendException e) {
            throw new ProtocolException(HttpServletResponse.SC_BAD_REQUEST, e.getMessage(), e);
        }
    }

    public Future<Void> deleteAlbum(UserId userId, String appId, String albumId,
        SecurityToken token) throws ProtocolException {
        String _userId = getUserId(userId, token);

        try {
            m_backend.deleteAlbum(_userId, appId, albumId);

            return ImmediateFuture.newInstance(null);
        }
        catch (BackendException e) {
            throw new ProtocolException(HttpServletResponse.SC_BAD_REQUEST, e.getMessage(), e);
        }
    }

    public Future<Album> getAlbum(UserId userId, String appId, Set<String> fields, String albumId, SecurityToken token)
        throws ProtocolException {
        String _userId = getUserId(userId, token);

        try {
            Album album = m_backend.getAlbum(_userId, appId, fields, albumId);

            return ImmediateFuture.newInstance(album);
        }
        catch (BackendException e) {
            throw new ProtocolException(HttpServletResponse.SC_BAD_REQUEST, e.getMessage(), e);
        }
    }

    public Future<RestfulCollection<Album>> getAlbums(Set<UserId> userIds,
        GroupId groupId, String appId, Set<String> fields,
        CollectionOptions options, SecurityToken token)
        throws ProtocolException {
        Set<String> _userIds = getUserList(userIds, token);
        String _groupId = getGroupId(groupId);
        QueryOptions _options = getQueryOptions(options);

        try {
            List<Album> albums = new ArrayList<Album>(m_backend.getAlbums(_userIds, _groupId, appId, fields, _options));

            return ImmediateFuture.newInstance(new RestfulCollection<Album>(albums));
        }
        catch (BackendException e) {
            throw new ProtocolException(HttpServletResponse.SC_BAD_REQUEST, e.getMessage(), e);
        }
    }

    public Future<RestfulCollection<Album>> getAlbums(UserId userId, String appId,
        Set<String> fields, CollectionOptions options,
        Set<String> albumIds, SecurityToken token) throws ProtocolException {
        String _userId = getUserId(userId, token);
        QueryOptions _options = getQueryOptions(options);

        try {
            List<Album> albums = new ArrayList<Album>(m_backend.getAlbums(_userId, appId, fields, _options, albumIds));

            return ImmediateFuture.newInstance(new RestfulCollection<Album>(albums));
        }
        catch (BackendException e) {
            throw new ProtocolException(HttpServletResponse.SC_BAD_REQUEST, e.getMessage(), e);
        }
    }

    public Future<Void> updateAlbum(UserId userId, String appId, Album album,
        String albumId, SecurityToken token) throws ProtocolException {
        String _userId = getUserId(userId, token);

        try {
            m_backend.updateAlbum(_userId, appId, album, albumId);

            return ImmediateFuture.newInstance(null);
        }
        catch (BackendException e) {
            throw new ProtocolException(HttpServletResponse.SC_BAD_REQUEST, e.getMessage(), e);
        }
    }
}
