/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package org.amdatu.opensocial.spi.bridge;

import static org.amdatu.opensocial.spi.bridge.BridgeUtils.getGroupId;
import static org.amdatu.opensocial.spi.bridge.BridgeUtils.getQueryOptions;
import static org.amdatu.opensocial.spi.bridge.BridgeUtils.getUserId;
import static org.amdatu.opensocial.spi.bridge.BridgeUtils.getUserList;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Future;

import javax.servlet.http.HttpServletResponse;

import org.amdatu.opensocial.spi.BackendException;
import org.amdatu.opensocial.spi.MediaItemBackendService;
import org.amdatu.opensocial.spi.QueryOptions;
import org.apache.shindig.auth.SecurityToken;
import org.apache.shindig.common.util.ImmediateFuture;
import org.apache.shindig.protocol.ProtocolException;
import org.apache.shindig.protocol.RestfulCollection;
import org.apache.shindig.social.opensocial.model.MediaItem;
import org.apache.shindig.social.opensocial.spi.CollectionOptions;
import org.apache.shindig.social.opensocial.spi.GroupId;
import org.apache.shindig.social.opensocial.spi.MediaItemService;
import org.apache.shindig.social.opensocial.spi.UserId;

/**
 * Bridges the {@link MediaItemService} SPI interface of Shindig to a less obtrusive API.
 */
public class MediaItemServiceBridge implements MediaItemService {

    // Injected by Felix DM...
    private volatile MediaItemBackendService m_backend;

    /**
     * Creates a new {@link MediaItemServiceBridge} instance.
     */
    public MediaItemServiceBridge() {
        // Nop
    }

    public Future<Void> createMediaItem(UserId userId, String appId, String albumId, MediaItem mediaItem, SecurityToken token) throws ProtocolException {
        String _user = getUserId(userId, token);

        try {
            m_backend.createMediaItem(_user, appId, albumId, mediaItem);

            return ImmediateFuture.newInstance(null);
        }
        catch (BackendException e) {
            throw new ProtocolException(HttpServletResponse.SC_BAD_REQUEST, e.getMessage(), e);
        }
    }

    public Future<Void> deleteMediaItem(UserId userId, String appId, String albumId, String mediaItemId, SecurityToken token) throws ProtocolException {
        String _user = getUserId(userId, token);

        try {
            m_backend.deleteMediaItem(_user, appId, albumId, mediaItemId);

            return ImmediateFuture.newInstance(null);
        }
        catch (BackendException e) {
            throw new ProtocolException(HttpServletResponse.SC_BAD_REQUEST, e.getMessage(), e);
        }
    }

    public Future<MediaItem> getMediaItem(UserId userId, String appId, String albumId, String mediaItemId, Set<String> fields, SecurityToken token) throws ProtocolException {
        String _user = getUserId(userId, token);
        
        try {
            MediaItem item = m_backend.getMediaItem(_user, appId, albumId, mediaItemId, fields);

            return ImmediateFuture.newInstance(item);
        }
        catch (BackendException e) {
            throw new ProtocolException(HttpServletResponse.SC_BAD_REQUEST, e.getMessage(), e);
        }
    }

    public Future<RestfulCollection<MediaItem>> getMediaItems(Set<UserId> userIds, GroupId groupId, String appId, Set<String> fields, CollectionOptions options, SecurityToken token) throws ProtocolException {
        Set<String> _userIds = getUserList(userIds, token);
        String _groupId = getGroupId(groupId);
        QueryOptions _options = getQueryOptions(options);

        try {
            List<MediaItem> items = new ArrayList<MediaItem>(m_backend.getMediaItems(_userIds, _groupId, appId, fields, _options));

            return ImmediateFuture.newInstance(new RestfulCollection<MediaItem>(items));
        }
        catch (BackendException e) {
            throw new ProtocolException(HttpServletResponse.SC_BAD_REQUEST, e.getMessage(), e);
        }
    }

    public Future<RestfulCollection<MediaItem>> getMediaItems(UserId userId, String appId, String albumId, Set<String> fields, CollectionOptions options, SecurityToken token) throws ProtocolException {
        String _user = getUserId(userId, token);
        QueryOptions _options = getQueryOptions(options);

        try {
            List<MediaItem> items = new ArrayList<MediaItem>(m_backend.getMediaItems(_user, appId, albumId, fields, _options));

            return ImmediateFuture.newInstance(new RestfulCollection<MediaItem>(items));
        }
        catch (BackendException e) {
            throw new ProtocolException(HttpServletResponse.SC_BAD_REQUEST, e.getMessage(), e);
        }
    }

    public Future<RestfulCollection<MediaItem>> getMediaItems(UserId userId, String appId, String albumId, Set<String> mediaItemIds, Set<String> fields, CollectionOptions options, SecurityToken token) throws ProtocolException {
        String _user = getUserId(userId, token);
        QueryOptions _options = getQueryOptions(options);

        try {
            List<MediaItem> items = new ArrayList<MediaItem>(m_backend.getMediaItems(_user, appId, albumId, mediaItemIds, fields, _options));

            return ImmediateFuture.newInstance(new RestfulCollection<MediaItem>(items));
        }
        catch (BackendException e) {
            throw new ProtocolException(HttpServletResponse.SC_BAD_REQUEST, e.getMessage(), e);
        }
    }

    public Future<Void> updateMediaItem(UserId userId, String appId, String albumId, String mediaItemId, MediaItem mediaItem, SecurityToken token) throws ProtocolException {
        String _user = getUserId(userId, token);

        try {
            m_backend.updateMediaItem(_user, appId, albumId, mediaItemId, mediaItem);

            return ImmediateFuture.newInstance(null);
        }
        catch (BackendException e) {
            throw new ProtocolException(HttpServletResponse.SC_BAD_REQUEST, e.getMessage(), e);
        }
    }
}
