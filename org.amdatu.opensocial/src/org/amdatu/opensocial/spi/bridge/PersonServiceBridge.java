/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package org.amdatu.opensocial.spi.bridge;

import static org.amdatu.opensocial.spi.bridge.BridgeUtils.getGroupId;
import static org.amdatu.opensocial.spi.bridge.BridgeUtils.getQueryOptions;
import static org.amdatu.opensocial.spi.bridge.BridgeUtils.getUserList;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Future;

import javax.servlet.http.HttpServletResponse;

import org.amdatu.opensocial.spi.BackendException;
import org.amdatu.opensocial.spi.PersonBackendService;
import org.amdatu.opensocial.spi.QueryOptions;
import org.apache.shindig.auth.SecurityToken;
import org.apache.shindig.common.util.ImmediateFuture;
import org.apache.shindig.protocol.ProtocolException;
import org.apache.shindig.protocol.RestfulCollection;
import org.apache.shindig.social.opensocial.model.Person;
import org.apache.shindig.social.opensocial.spi.CollectionOptions;
import org.apache.shindig.social.opensocial.spi.GroupId;
import org.apache.shindig.social.opensocial.spi.PersonService;
import org.apache.shindig.social.opensocial.spi.UserId;

/**
 * Bridges the {@link PersonService} SPI interface of Shindig to a less obtrusive API.
 */
public class PersonServiceBridge implements PersonService {

    // Injected by Felix DM
    private PersonBackendService m_backend;

    /**
     * Creates a new {@link PersonServiceBridge} implementation.
     */
    public PersonServiceBridge() {
        // Nop
    }

    @Override
    public Future<RestfulCollection<Person>> getPeople(Set<UserId> userIds, GroupId groupId, CollectionOptions options,
        Set<String> fields, SecurityToken token) throws ProtocolException {
        // Convert the input to our own backend's expectations...
        Set<String> _userIds = getUserList(userIds, token);
        String _groupId = getGroupId(groupId);
        QueryOptions _options = getQueryOptions(options);

        try {
            List<Person> people = new ArrayList<Person>(m_backend.getPeople(_userIds, _groupId, fields, _options));

            // Determine the total result size...
            int totalResultSize = m_backend.countPeople(_userIds, _groupId, fields, _options);

            return ImmediateFuture.newInstance(new RestfulCollection<Person>(people, options.getFirst(),
                totalResultSize, options.getMax()));
        }
        catch (BackendException e) {
            throw new ProtocolException(HttpServletResponse.SC_BAD_REQUEST, e.getMessage(), e);
        }
    }

    @Override
    public Future<Person> getPerson(UserId id, Set<String> fields, SecurityToken token) throws ProtocolException {
        // Convert the input to our own backend's expectations...
        String _id = id.getUserId(token);

        try {
            return ImmediateFuture.newInstance(m_backend.getPerson(_id, fields));
        }
        catch (BackendException e) {
            throw new ProtocolException(HttpServletResponse.SC_BAD_REQUEST, e.getMessage(), e);
        }
    }

    @Override
    public Future<Person> updatePerson(UserId id, Person person, SecurityToken token) throws ProtocolException {
        // Convert the input to our own backend's expectations...
        String _id = id.getUserId(token); // person to update
        String _viewer = token.getViewerId();

        if (!viewerCanUpdatePerson(_viewer, _id)) {
            throw new ProtocolException(HttpServletResponse.SC_FORBIDDEN, "User '" + _viewer
                + "' does not have enough privileges to update person '" + _id + "'");
        }

        try {
            return ImmediateFuture.newInstance(m_backend.updatePerson(_id, person));
        }
        catch (BackendException e) {
            throw new ProtocolException(HttpServletResponse.SC_BAD_REQUEST, e.getMessage(), e);
        }
    }

    /**
     * Verifies if a viewer is allowed to update the given person record.
     */
    private boolean viewerCanUpdatePerson(String viewer, String person) {
        // A person can only update his own personal data (by default)
        // if you wish to allow other people to update the personal data of the user
        // you should change the current function
        return viewer.equals(person);
    }
}
