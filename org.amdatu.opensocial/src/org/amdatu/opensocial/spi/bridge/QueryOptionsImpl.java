/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package org.amdatu.opensocial.spi.bridge;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;

import org.amdatu.opensocial.spi.QueryOptions;
import org.apache.shindig.social.opensocial.spi.CollectionOptions;

/**
 * Bridges the {@link org.apache.shindig.social.opensocial.spi.CollectionOptions}.
 */
class QueryOptionsImpl implements QueryOptions {

    private final String m_filter;
    private final String m_filterValue;
    private final String m_filterOperation;
    private final int m_first;
    private final int m_max;
    private final Map<String, String> m_optionalParameters;
    private final String m_sortBy;
    private final String m_sortOrder;
    private final Date m_updatedSince;

    /**
     * Creates a new {@link QueryOptionsImpl} instance based on the given collection options from Shindig.
     */
    public QueryOptionsImpl(CollectionOptions options) {
        m_filter = options.getFilter();
        m_filterValue = options.getFilterValue();
        m_filterOperation = options.getFilterOperation().name();
        m_first = options.getFirst();
        m_max = options.getMax();
        m_optionalParameters = new HashMap<String, String>(options.getOptionalParameter());
        m_sortBy = options.getSortBy();
        m_sortOrder = options.getSortOrder().name();
        m_updatedSince = options.getUpdatedSince();
    }

    @Override
    public String getFilter() {
        return m_filter;
    }

    @Override
    public String getFilterOperation() {
        return m_filterOperation;
    }

    @Override
    public String getFilterValue() {
        return m_filterValue;
    }

    @Override
    public int getFirst() {
        return m_first;
    }

    @Override
    public int getMax() {
        return m_max;
    }

    @Override
    public Map<String, String> getOptionalParameters() {
        return m_optionalParameters;
    }

    @Override
    public String getSortBy() {
        return m_sortBy;
    }

    @Override
    public String getSortOrder() {
        return m_sortOrder;
    }

    @Override
    public Date getUpdatedSince() {
        return m_updatedSince;
    }
}
