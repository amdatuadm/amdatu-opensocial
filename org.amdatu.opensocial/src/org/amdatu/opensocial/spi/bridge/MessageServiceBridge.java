/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package org.amdatu.opensocial.spi.bridge;

import static org.amdatu.opensocial.spi.bridge.BridgeUtils.getQueryOptions;
import static org.amdatu.opensocial.spi.bridge.BridgeUtils.getUserId;

import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.Future;

import javax.servlet.http.HttpServletResponse;

import org.amdatu.opensocial.spi.BackendException;
import org.amdatu.opensocial.spi.MessageBackendService;
import org.amdatu.opensocial.spi.QueryOptions;
import org.apache.shindig.auth.SecurityToken;
import org.apache.shindig.common.util.ImmediateFuture;
import org.apache.shindig.protocol.ProtocolException;
import org.apache.shindig.protocol.RestfulCollection;
import org.apache.shindig.social.opensocial.model.Message;
import org.apache.shindig.social.opensocial.model.MessageCollection;
import org.apache.shindig.social.opensocial.spi.CollectionOptions;
import org.apache.shindig.social.opensocial.spi.MessageService;
import org.apache.shindig.social.opensocial.spi.UserId;

/**
 * Bridges the {@link MessageService} SPI interface of Shindig to a less obtrusive API.
 */
public class MessageServiceBridge implements MessageService {

    // Injected by Felix DM
    private MessageBackendService m_backend;

    /**
     * Creates a new {@link MessageServiceBridge} instance.
     */
    public MessageServiceBridge() {
        // Nop
    }

    public Future<Void> createMessage(UserId userId, String appId, String msgCollId, Message message,
        SecurityToken token) throws ProtocolException {
        String _user = getUserId(userId, token);

        try {
            m_backend.createMessage(_user, appId, msgCollId, message);

            return ImmediateFuture.newInstance(null);
        }
        catch (BackendException e) {
            throw new ProtocolException(HttpServletResponse.SC_BAD_REQUEST, e.getMessage(), e);
        }
    }

    public Future<MessageCollection> createMessageCollection(UserId userId, MessageCollection msgCollection,
        SecurityToken token) throws ProtocolException {
        String _user = getUserId(userId, token);

        try {
            MessageCollection collection = m_backend.createMessageCollection(_user, msgCollection);

            return ImmediateFuture.newInstance(collection);
        }
        catch (BackendException e) {
            throw new ProtocolException(HttpServletResponse.SC_BAD_REQUEST, e.getMessage(), e);
        }
    }

    public Future<Void> deleteMessageCollection(UserId userId, String msgCollId, SecurityToken token)
        throws ProtocolException {
        String _user = getUserId(userId, token);

        try {
            m_backend.deleteMessageCollection(_user, msgCollId);

            return ImmediateFuture.newInstance(null);
        }
        catch (BackendException e) {
            throw new ProtocolException(HttpServletResponse.SC_BAD_REQUEST, e.getMessage(), e);
        }
    }

    public Future<Void> deleteMessages(UserId userId, String msgCollId, List<String> ids, SecurityToken token)
        throws ProtocolException {
        String _user = getUserId(userId, token);

        try {
            m_backend.deleteMessages(_user, msgCollId, ids);

            return ImmediateFuture.newInstance(null);
        }
        catch (BackendException e) {
            throw new ProtocolException(HttpServletResponse.SC_BAD_REQUEST, e.getMessage(), e);
        }
    }

    public Future<RestfulCollection<MessageCollection>> getMessageCollections(UserId userId, Set<String> fields,
        CollectionOptions options, SecurityToken token) throws ProtocolException {
        String _user = getUserId(userId, token);
        QueryOptions _options = getQueryOptions(options);

        try {
            List<MessageCollection> collection =
                new ArrayList<MessageCollection>(m_backend.getMessageCollections(_user, fields, _options));

            return ImmediateFuture.newInstance(new RestfulCollection<MessageCollection>(collection));
        }
        catch (BackendException e) {
            throw new ProtocolException(HttpServletResponse.SC_BAD_REQUEST, e.getMessage(), e);
        }
    }

    public Future<RestfulCollection<Message>> getMessages(UserId userId, String msgCollId, Set<String> fields,
        List<String> msgIds, CollectionOptions options, SecurityToken token) throws ProtocolException {
        String _user = getUserId(userId, token);
        QueryOptions _options = getQueryOptions(options);

        try {
            List<Message> collection =
                new ArrayList<Message>(m_backend.getMessages(_user, msgCollId, fields, msgIds, _options));

            return ImmediateFuture.newInstance(new RestfulCollection<Message>(collection));
        }
        catch (BackendException e) {
            throw new ProtocolException(HttpServletResponse.SC_BAD_REQUEST, e.getMessage(), e);
        }
    }

    public Future<Void> modifyMessage(UserId userId, String msgCollId, String messageId, Message message,
        SecurityToken token) throws ProtocolException {
        String _user = getUserId(userId, token);

        try {
            m_backend.modifyMessage(_user, msgCollId, messageId, message);

            return ImmediateFuture.newInstance(null);
        }
        catch (BackendException e) {
            throw new ProtocolException(HttpServletResponse.SC_BAD_REQUEST, e.getMessage(), e);
        }
    }

    public Future<Void> modifyMessageCollection(UserId userId, MessageCollection msgCollection, SecurityToken token)
        throws ProtocolException {
        String _user = getUserId(userId, token);

        try {
            m_backend.modifyMessageCollection(_user, msgCollection);

            return ImmediateFuture.newInstance(null);
        }
        catch (BackendException e) {
            throw new ProtocolException(HttpServletResponse.SC_BAD_REQUEST, e.getMessage(), e);
        }
    }
}
