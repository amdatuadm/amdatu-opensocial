/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package org.amdatu.opensocial.spi;

import java.util.Collection;

import org.apache.shindig.social.opensocial.model.ActivityEntry;

/**
 * Provides a simple facade for <tt>org.apache.shindig.social.opensocial.spi.ActivityStreamService</tt>.
 */
public interface ActivityStreamBackendService {

    /**
     * Creates the passed in activity for the passed in user and group. Once createActivity is called,
     * getActivities will be able to return the Activity.
     * 
     * @param userId The id of the person to create the activity for.
     * @param groupId The group.
     * @param appId The app id.
     * @param fields The fields to return.
     * @param activity The activity to create.
     * @param token A valid SecurityToken
     * @return a response item containing any errors
     * @throws org.apache.shindig.protocol.BackendException if any.
     */
    ActivityEntry createActivityEntry(String userId, String groupId, String appId,
        Collection<String> fields, ActivityEntry activity) throws BackendException;

    /**
     * Deletes the activity for the passed in user and group that corresponds to the activityId.
     * 
     * @param userId The user.
     * @param groupId The group.
     * @param appId The app id.
     * @param activityIds A list of activity ids to delete.
     * @param token A valid SecurityToken.
     * @return a response item containing any errors
     * @throws org.apache.shindig.protocol.BackendException if any.
     */
    void deleteActivityEntries(String userId, String groupId, String appId,
        Collection<String> activityIds) throws BackendException;

    /**
     * Returns a list of activities that correspond to the passed in users and group.
     * 
     * @param userIds The set of ids of the people to fetch activities for.
     * @param groupId Indicates whether to fetch activities for a group.
     * @param appId The app id.
     * @param fields The fields to return. Empty set implies all
     * @param options The sorting/filtering/pagination options
     * @param token A valid SecurityToken
     * @return a response item with the list of activities.
     * @throws BackendException if any.
     */
    Collection<ActivityEntry> getActivityEntries(Collection<String> userIds, String groupId, String appId,
        Collection<String> fields, QueryOptions options) throws BackendException;

    /**
     * Returns a set of activities for the passed in user and group that corresponds to a list of
     * activityIds.
     * 
     * @param userId The set of ids of the people to fetch activities for.
     * @param groupId Indicates whether to fetch activities for a group.
     * @param appId The app id.
     * @param fields The fields to return. Empty set implies all
     * @param options The sorting/filtering/pagination options
     * @param activityIds The set of activity ids to fetch.
     * @param token A valid SecurityToken
     * @return a response item with the list of activities.
     * @throws BackendException if any.
     */
    Collection<ActivityEntry> getActivityEntries(String userId, String groupId,
        String appId, Collection<String> fields, QueryOptions options, Collection<String> activityIds)
        throws BackendException;

    /**
     * Returns an activity for the passed in user and group that corresponds to a single
     * activityId.
     * 
     * @param userId The set of ids of the people to fetch activities for.
     * @param groupId Indicates whether to fetch activities for a group.
     * @param appId The app id.
     * @param fields The fields to return. Empty set implies all
     * @param activityId The activity id to fetch.
     * @param token A valid SecurityToken
     * @return a response item with the list of activities.
     * @throws BackendException if any.
     */
    ActivityEntry getActivityEntry(String userId, String groupId, String appId,
        Collection<String> fields, String activityId)
        throws BackendException;

    /**
     * Updates the specified Activity.
     * 
     * @param userId The id of the person to update the activity for
     * @param groupId The group
     * @param appId The app id
     * @param fields The fields to return
     * @param activity The updated activity
     * @param activityId The id of the existing activity to update
     * @param token A valid SecurityToken
     * @return a response item containing any errors
     * @throws org.apache.shindig.protocol.BackendException if any
     */
    ActivityEntry updateActivityEntry(String userId, String groupId, String appId,
        Collection<String> fields, ActivityEntry activity, String activityId) throws BackendException;
}
