/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package org.amdatu.opensocial.spi;

import java.util.Collection;

import org.apache.shindig.social.opensocial.model.Person;

/**
 * Provides a simple facade for <tt>org.apache.shindig.social.opensocial.spi.PersonService</tt>.
 */
public interface PersonBackendService {

    /**
     * Returns the total number of people that correspond to the passed in person IDs.
     * 
     * @param userIds the set with IDs of users to retrieve;
     * @param groupId the ID of the group the retrieved users relate to, such as "@all", "@friends", or "@self";
     * @param fields denotes which fields of the requested people are retrieved. An empty set implies all fields will be retrieved;
     * @param options provides details on how to filter, sort and paginate the collection being retrieved, can be <code>null</code>.
     * @return a collection of people, never <code>null</code>.
     * @throws BackendException in case the backend failed somehow in the request.
     */
    int countPeople(Collection<String> userIds, String groupId, Collection<String> fields, QueryOptions options) throws BackendException;

    /**
     * Returns a list of people that correspond to the passed in person IDs.
     * 
     * @param userIds the set with IDs of users to retrieve;
     * @param groupId the ID of the group the retrieved users relate to, such as "@all", "@friends", or "@self";
     * @param fields denotes which fields of the requested people are retrieved. An empty set implies all fields will be retrieved;
     * @param options provides details on how to filter, sort and paginate the collection being retrieved, can be <code>null</code>.
     * @return a collection of people, never <code>null</code>.
     * @throws BackendException in case the backend failed somehow in the request.
     */
    Collection<Person> getPeople(Collection<String> userIds, String groupId, Collection<String> fields, QueryOptions options) throws BackendException;

    /**
     * Returns a person with the requested user ID.
     * 
     * @param id the ID of the person to retrieve;
     * @param fields denotes which fields of the requested person are retrieved. An empty set implies all fields will be retrieved;
     * @return the requested person, or <code>null</code> if no such person exists.
     * @throws BackendException in case the backend failed somehow in the request.
     */
    Person getPerson(String id, Collection<String> fields) throws BackendException;

    /**
     * Updates the fields of a given person.
     * 
     * @param id the ID of the person to retrieve;
     * @param person the person to update, cannot be <code>null</code>;
     * @return the updated person, never <code>null</code>.
     * @throws BackendException in case the backend failed somehow in the request.
     */
    Person updatePerson(String id, Person person) throws BackendException;

}
