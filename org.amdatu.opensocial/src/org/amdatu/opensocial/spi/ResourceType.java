/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */

package org.amdatu.opensocial.spi;

import java.util.Arrays;

/**
 * Denotes a type of resource, such as person, or address.
 * <p>
 * Implementation note: the field names are based on the OpenSocial 2.5.
 * </p>
 */
public enum ResourceType {
    /** Describes an account held by a person. */
    ACCOUNT("domain", "userId", "username"),

    /** An ActionLink encompasses an action that a user may perform against an actionable resource. */
    ACTION_LINK("caption", "httpVerb", "target"),

    /**
     * An activity represents a short summary or notification of a timestamped event, often with pointers for more information.
     * 
     * @deprecated As of OpenSocial 2.0. Will be removed in future version; use Activity Streams instead.
     */
    ACTIVITY("appId", "body", "bodyId", "externalId", "id", "mediaItems", "postedTime", "priority",
        "streamFaviconUrl", "streamSourceUrl", "streamTitle", "streamUrl", "templateParams", "title", "url",
        "userId"),

    /** An object is a thing, real or imaginary, which participates in an activity. */
    ACTIVITY_OBJECT("attachments", "author", "content", "displayName", "downstreamDuplicates", "id", "image",
        "objectType", "published", "summary", "updated", "upstreamDuplicates", "url"),

    /** In its simplest form, an activity consists of an actor, a verb, an an object, and a target. */
    ACTIVITY_ENTRY("actionLinks", "actor", "content", "deliverTo", "embed", "generator", "icon", "id", "object",
        "published", "provider", "target", "title", "updated", "url", "verb"),

    /** The components of a physical mailing address. */
    ADDRESS("building", "country", "floor", "formatted", "latitude", "locality", "longitude", "postalCode",
        "region", "streetAddress", "type"),

    /** Albums support collections of media items (video, image, sound). */
    ALBUM("description", "id", "location", "mediaItemCount", "mediaMimeType", "mediaType", "ownerId",
        "thumbnailUrl", "title"),

    /** Defines a data store that applications can use to read and write user-specific data. */
    APP_DATA( /* any field can be put in this container */),

    /** The app Id is an Object-Id for the application. */
    APP_ID(),

    /** Each FieldMetadata instance represents a field that MAY be present in a particular entity of the containing object type. */
    FIELD_METADATA("availability", "description", "editable", "name", "required", "since", "type"),

    /** Describes a generic file or document. */
    FILE("author", "displayName", "fileUrl", "id", "published", "mimeType", "updated", "url"),

    /** Groups are owned by people, and are used to tag or categorize people and their relationships. */
    GROUP("id", "description", "title"),

    /** The group Id must only contain alphanumeric (A-Za-z0-9) characters, underscore(_), dot(.) or dash(-), and must uniquely identify the group in a container. */
    GROUP_ID(),

    /** Represents images, movies, and audio. */
    MEDIA_ITEM("album_id", "created", "description", "duration", "file_size", "id", "language", "last_updated",
        "location", "mime_type", "num_comments", "num_views", "num_votes", "rating", "start_time", "tagged_people",
        "tags", "thumbnail_url", "title", "type", "url"),

    /** Some types of objects may have an alternative visual representation in the form of an image, video or embedded HTML fragments. A Media Link represents a hyperlink to such resources. */
    MEDIA_LINK("duration", "height", "mediaItemId", "url", "width"),

    /** Represents a message. */
    MESSAGE("appUrl", "body", "bodyId", "collectionIds", "id", "inReplyTo", "recipients", "replies", "senderId",
        "status", "timeSent", "title", "titleId", "type", "updated", "urls"),

    /** The components of the person's real name. */
    NAME("familyName", "formatted", "givenName", "honorificPrefix", "honorificSuffix", "middleName",
        "pronunciation", "pronunciationUrl"),

    /** Each ObjectMetadata instance represents all of the metadata associated with a particular object type. */
    OBJECT_METADATA("description", "fields", "name", "resourceLinks", "since"),

    /** Describes a current or past organizational affiliation of this contact. */
    ORGANIZATION("address", "department", "description", "endDate", "field", "location", "name", "salary",
        "startDate", "subfield", "title", "type", "webpage"),

    /** Represents a person. */
    PERSON("aboutMe", "accounts", "activities", "addresses", "age", "alternateNames", "anniversary", "appData",
        "birthday", "bodyType", "books", "cars", "children", "connected", "contactPreference", "displayName", "dn",
        "drinker", "emails", "ethnicity", "fashion", "food", "gender", "happiestWhen", "hasApp", "heroes", "humor",
        "id", "ims", "interests", "jobInterests", "languagesSpoken", "livingArrangement", "location", "lookingFor",
        "movies", "music", "name", "nativeName", "networkPresence", "nickname", "note", "organizations",
        "orgIdentifier", "pets", "phoneNumbers", "photos", "politicalViews", "preferredName", "preferredUsername",
        "profileSong", "profileUrl", "profileVideo", "published", "quotes", "relationships", "relationshipStatus",
        "religion", "romance", "scaredOf", "sexualOrientation", "status", "tags", "thumbnailUrl", "updated",
        "urls", "utcOffset"),

    /** Each PropertyMetadata instance represents a configuration value for this social container. */
    PROPERTY_METADATA("description", "name", "type", "value"), ;

    private final String[] m_fieldNames;

    /**
     * Creates a new {@link ResourceType} instance.
     * 
     * @param fieldNames the field names that can occur in this type of resource.
     */
    private ResourceType(String... fieldNames) {
        m_fieldNames = Arrays.copyOf(fieldNames, fieldNames.length);
    }

    /**
     * Returns the possible field names for this resource type.
     * 
     * @return an array of possible field names, sorted in ascending (natural) order, never <code>null</code>.
     */
    public final String[] getFieldNames() {
        return m_fieldNames;
    }
}
