/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package org.amdatu.opensocial.module;

import static org.ops4j.peaberry.Peaberry.service;
import net.oauth.OAuthValidator;

import org.apache.shindig.auth.AuthenticationHandlerProvider;
import org.apache.shindig.common.servlet.ParameterFetcher;
import org.apache.shindig.protocol.DataServiceServletFetcher;
import org.apache.shindig.protocol.conversion.BeanConverter;
import org.apache.shindig.protocol.conversion.BeanJsonConverter;
import org.apache.shindig.protocol.conversion.BeanXStreamConverter;
import org.apache.shindig.protocol.conversion.xstream.XStreamConfiguration;
import org.apache.shindig.social.core.oauth.GuiceAuthenticationHandlerProvider;
import org.apache.shindig.social.core.oauth.OAuthValidatorProvider;
import org.apache.shindig.social.core.oauth2.OAuth2DataService;
import org.apache.shindig.social.core.oauth2.OAuth2Service;
import org.apache.shindig.social.core.util.BeanXStreamAtomConverter;
import org.apache.shindig.social.core.util.xstream.XStream081Configuration;
import org.apache.shindig.social.opensocial.oauth.OAuthDataStore;
import org.apache.shindig.social.opensocial.service.ActivityHandler;
import org.apache.shindig.social.opensocial.service.ActivityStreamHandler;
import org.apache.shindig.social.opensocial.service.AlbumHandler;
import org.apache.shindig.social.opensocial.service.AppDataHandler;
import org.apache.shindig.social.opensocial.service.GroupHandler;
import org.apache.shindig.social.opensocial.service.MediaItemHandler;
import org.apache.shindig.social.opensocial.service.MessageHandler;
import org.apache.shindig.social.opensocial.service.PersonHandler;
import org.apache.shindig.social.opensocial.spi.ActivityService;
import org.apache.shindig.social.opensocial.spi.ActivityStreamService;
import org.apache.shindig.social.opensocial.spi.AlbumService;
import org.apache.shindig.social.opensocial.spi.AppDataService;
import org.apache.shindig.social.opensocial.spi.GroupService;
import org.apache.shindig.social.opensocial.spi.MessageService;
import org.apache.shindig.social.opensocial.spi.PersonService;

import com.google.inject.AbstractModule;
import com.google.inject.Singleton;
import com.google.inject.multibindings.Multibinder;
import com.google.inject.name.Names;

/**
 * Provides a custom module definition for {@link org.apache.shindig.social.core.config.SocialApiGuiceModule} in order
 * to make use of OSGi service dynamics.
 */
public class SocialApiModule extends AbstractModule {

    @Override
    protected void configure() {
        // @formatter:off
		// Non-OSGi
		bind(ParameterFetcher.class)
			.annotatedWith(Names.named("DataServiceServlet"))
			.to(DataServiceServletFetcher.class);

		// Non-OSGi
		bind(XStreamConfiguration.class)
			.to(XStream081Configuration.class);

		// Non-OSGi
		bind(BeanConverter.class)
			.annotatedWith(Names.named("shindig.bean.converter.xml"))
			.to(BeanXStreamConverter.class);

		// Non-OSGi
		bind(BeanConverter.class)
			.annotatedWith(Names.named("shindig.bean.converter.json"))
			.to(BeanJsonConverter.class);

		// Non-OSGi
		bind(BeanConverter.class)
			.annotatedWith(Names.named("shindig.bean.converter.atom"))
			.to(BeanXStreamAtomConverter.class);

		// Non-OSGi
		Multibinder<Object> handlerBinder = Multibinder.newSetBinder(binder(),
				Object.class, Names.named("org.apache.shindig.handlers"));
		
		Class[] handlers = getHandlers();
		for (int i = 0; i < handlers.length; i += 2) {
			Class handlerClass = handlers[i];
			Class serviceClass = handlers[i + 1];

			handlerBinder.addBinding().toInstance(handlerClass);

			bind(serviceClass).toProvider(service(serviceClass).single());
		}

		// Non-OSGi
	    bind(OAuthValidator.class)
	        .toProvider(OAuthValidatorProvider.class)
	        .in(Singleton.class);

		// Implemented by: OAuth2DataServiceImpl
        bind(OAuth2DataService.class)
            .toProvider(service(OAuth2DataService.class)
                .single());

        // Implemented by: OAuth2ServiceImpl
        bind(OAuth2Service.class)
            .toProvider(service(OAuth2Service.class)
                .single());
        
        // Implemented by: SampleOAuthDataStore
        bind(OAuthDataStore.class)
            .toProvider(service(OAuthDataStore.class)
                .single());

		// Non-OSGi
	    bind(AuthenticationHandlerProvider.class)
	        .to(GuiceAuthenticationHandlerProvider.class);
		// @formatter:on
    }

    /**
     * Returns all REST/RPC handlers and their accompanying backend services.
     * 
     * @return an array of arrays
     */
    @SuppressWarnings("unchecked")
    protected Class[] getHandlers() {
        return new Class[] { ActivityHandler.class, ActivityService.class,
            AppDataHandler.class, AppDataService.class,
            PersonHandler.class, PersonService.class, MessageHandler.class,
            MessageService.class, AlbumHandler.class, AlbumService.class,
            MediaItemHandler.class, MediaItemHandler.class,
            ActivityStreamHandler.class, ActivityStreamService.class,
            GroupHandler.class, GroupService.class };
    }
}
