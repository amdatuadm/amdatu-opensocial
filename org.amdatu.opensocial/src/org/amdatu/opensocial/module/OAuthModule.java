/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package org.amdatu.opensocial.module;

import static org.ops4j.peaberry.Peaberry.service;
import static org.ops4j.peaberry.util.Filters.ldap;

import org.apache.shindig.common.crypto.BlobCrypter;
import org.apache.shindig.gadgets.oauth.OAuthFetcherConfig;
import org.apache.shindig.gadgets.oauth.OAuthRequest;
import org.apache.shindig.gadgets.oauth.OAuthStore;

import com.google.inject.AbstractModule;
import com.google.inject.name.Names;

/**
 * OSGi replacement for {@link org.apache.shindig.gadgets.oauth.OAuthModule}.
 */
public class OAuthModule extends AbstractModule {

    @Override
    protected void configure() {
        // @formatter:off
        bind(BlobCrypter.class)
            .annotatedWith(Names.named(OAuthFetcherConfig.OAUTH_STATE_CRYPTER))
            .toProvider(service(BlobCrypter.class)
                .filter(ldap("(name=" + OAuthFetcherConfig.OAUTH_STATE_CRYPTER + ")"))
                .single());
        bind(OAuthStore.class)
            .toProvider(service(OAuthStore.class)
                .single());
        bind(OAuthRequest.class) // XXX not an interface!
            .toProvider(service(OAuthRequest.class)
                .single());
        // @formatter:on
    }
}
