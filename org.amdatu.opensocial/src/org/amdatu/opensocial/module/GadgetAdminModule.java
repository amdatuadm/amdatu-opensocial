/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package org.amdatu.opensocial.module;

import org.apache.shindig.gadgets.admin.GadgetAdminModule.GadgetAdminStoreProvider;
import org.apache.shindig.gadgets.admin.GadgetAdminStore;

import com.google.inject.AbstractModule;

/**
 * Provides an OSGi-capable implementation of {@link org.apache.shindig.gadgets.admin.GadgetAdminModule}.
 */
public class GadgetAdminModule extends AbstractModule {

    @Override
    protected void configure() {
        // @formatter:off
        bind(GadgetAdminStore.class).toProvider(GadgetAdminStoreProvider.class);
        // @formatter:on
    }
}
