/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package org.amdatu.opensocial.module;

import static org.ops4j.peaberry.Peaberry.service;
import static org.ops4j.peaberry.util.Filters.ldap;

import org.apache.shindig.common.crypto.BlobCrypter;
import org.apache.shindig.gadgets.oauth2.OAuth2FetcherConfig;
import org.apache.shindig.gadgets.oauth2.OAuth2Message;
import org.apache.shindig.gadgets.oauth2.OAuth2Request;
import org.apache.shindig.gadgets.oauth2.OAuth2RequestParameterGenerator;
import org.apache.shindig.gadgets.oauth2.OAuth2Store;
import org.apache.shindig.gadgets.oauth2.persistence.OAuth2Cache;
import org.apache.shindig.gadgets.oauth2.persistence.OAuth2Encrypter;
import org.apache.shindig.gadgets.oauth2.persistence.OAuth2Persister;

import com.google.inject.AbstractModule;
import com.google.inject.name.Names;

/**
 * OSGi replacement for {@link org.apache.shindig.gadgets.oauth2.persistence.sample.OAuth2PersistenceModule}, ...
 */
public class OAuth2Module extends AbstractModule {

    @Override
    protected void configure() {
        // @formatter:off
        bind(OAuth2Persister.class)
            .toProvider(service(OAuth2Persister.class)
                .single());
        bind(OAuth2Cache.class)
            .toProvider(service(OAuth2Cache.class)
                .single());
        bind(OAuth2Encrypter.class)
            .toProvider(service(OAuth2Encrypter.class)
                .single());
        
        bind(OAuth2Store.class)
            .toProvider(service(OAuth2Store.class)
                .single());
        bind(OAuth2Request.class)
            .toProvider(service(OAuth2Request.class)
                .single());
        bind(OAuth2RequestParameterGenerator.class)
            .toProvider(service(OAuth2RequestParameterGenerator.class)
                .single());
        // Used for encrypting client-side OAuth2 state.
        bind(BlobCrypter.class)
            .annotatedWith(Names.named(OAuth2FetcherConfig.OAUTH2_STATE_CRYPTER))
            .toProvider(service(BlobCrypter.class)
                .filter(ldap("(name=" + OAuth2FetcherConfig.OAUTH2_STATE_CRYPTER + ")"))
                .single());

        bind(OAuth2Message.class)
            .toProvider(service(OAuth2Message.class)
                .single());
        
        // This module installs all kinds of Guice-specific providers...
        install(new org.apache.shindig.gadgets.oauth2.handler.OAuth2HandlerModule());
        // @formatter:on
    }
}
