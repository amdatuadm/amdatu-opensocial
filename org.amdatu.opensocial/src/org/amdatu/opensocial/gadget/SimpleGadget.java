/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package org.amdatu.opensocial.gadget;

import java.util.Arrays;

/**
 * Provides a default implementation of {@link Gadget} for convenience.
 */
public class SimpleGadget implements Gadget {

    private final String m_id;
    private final String[] m_apps;

    /**
     * Creates a new {@link SimpleGadget} instance.
     * 
     * @param id the identifier of this gadget;
     * @param apps the gadget specifications of this gadget, cannot be <code>null</code> or empty.
     */
    public SimpleGadget(String id, String... apps) {
        if (id == null || "".equals(id.trim())) {
            throw new IllegalArgumentException("ID cannot be null or empty!");
        }
        if (apps == null || apps.length == 0) {
            throw new IllegalArgumentException("Apps cannot be null or empty!");
        }
        m_id = id;
        m_apps = Arrays.copyOf(apps, apps.length);
    }

    @Override
    public boolean equals(Object object) {
        if (this == object)
            return true;
        if (object == null || getClass() != object.getClass())
            return false;

        SimpleGadget other = (SimpleGadget) object;
        if (m_id == null) {
            if (other.m_id != null) {
                return false;
            }
        }
        else if (!m_id.equals(other.m_id)) {
            return false;
        }
        if (m_apps == null) {
            if (other.m_apps != null) {
                return false;
            }
        }
        else if (!m_apps.equals(other.m_apps)) {
            return false;
        }
        return true;
    }
    
    @Override
    public String getId() {
        return m_id;
    }

    @Override
    public String[] getApps() {
        return m_apps;
    }

    @Override
    public int hashCode() {
        final int prime = 31;
        int result = 1;
        result = prime * result + ((m_id == null) ? 0 : m_id.hashCode());
        result = prime * result + ((m_apps == null) ? 0 : m_apps.hashCode());
        return result;
    }
}
