/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package org.amdatu.opensocial.gadget;

import java.util.Collection;

/**
 * Provides access to all registered {@link Gadget}s.
 */
public interface GadgetProvider {
    /**
     * Returns a collection of all registered gadgets.
     * 
     * @param filter the optional (LDAP-style) filter to apply to the gadgets,
     *        which will return only gadgets whose service properties match the
     *        given filter. May be <code>null</code> in which case all gadgets
     *        will be returned.
     * @return a collection of gadgets, never <code>null</code>.
     */
    Collection<Gadget> getGadgets(String filter);
}
