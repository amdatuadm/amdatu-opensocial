/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package org.amdatu.opensocial.gadget.impl;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Dictionary;
import java.util.Hashtable;
import java.util.List;
import java.util.Map.Entry;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.amdatu.opensocial.gadget.Gadget;
import org.amdatu.opensocial.gadget.GadgetProvider;
import org.osgi.framework.Filter;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.InvalidSyntaxException;
import org.osgi.framework.ServiceReference;

/**
 * Provides an implementation of a {@link GadgetProvider} that allows {@link Gadget}s to be registered in a whiteboard-style manner.
 */
public class GadgetProviderImpl implements GadgetProvider {
    
    private static final String KEY_ID = "id";
    
    private final ConcurrentMap<Gadget, ServiceReference> m_gadgets;

    /**
     * Creates a new {@link GadgetProviderImpl} instance.
     */
    public GadgetProviderImpl() {
        m_gadgets = new ConcurrentHashMap<Gadget, ServiceReference>();
    }

    /**
     * Adds (or registers) a given gadget to this provider.
     * 
     * @param serviceRef the service reference of the gadget, cannot be <code>null</code>;
     * @param gadget the gadget to add, cannot be <code>null</code>.
     */
    public void addGadget(ServiceReference serviceRef, Gadget gadget) {
        m_gadgets.putIfAbsent(gadget, serviceRef);
    }

    @Override
    public Collection<Gadget> getGadgets(String filterStr) {
        if (filterStr == null || "".equals(filterStr.trim())) {
            return new ArrayList<Gadget>(m_gadgets.keySet());
        }

        try {
            Filter filter = FrameworkUtil.createFilter(filterStr);

            Collection<Entry<Gadget, ServiceReference>> entrySet = new ArrayList<Entry<Gadget,ServiceReference>>(m_gadgets.entrySet());

            List<Gadget> result = new ArrayList<Gadget>();
            for (Entry<Gadget, ServiceReference> entry : entrySet) {
                Dictionary props = getProperties(entry.getKey(), entry.getValue());
                if (filter.match(props)) {
                    result.add(entry.getKey());
                }
            }

            return result;
        }
        catch (InvalidSyntaxException e) {
            throw new RuntimeException("Invalid filter!", e);
        }
    }

    /**
     * Removes a given gadget from this provider.
     * 
     * @param serviceRef the service reference of the gadget, cannot be <code>null</code>;
     * @param gadget the gadget to remove, cannot be <code>null</code>.
     */
    public void removeGadget(ServiceReference serviceRef, Gadget gadget) {
        m_gadgets.remove(gadget, serviceRef);
    }
    
    /**
     * Returns a dictionary unifying the gadget ID and its service properties.
     */
    private Dictionary getProperties(Gadget gadget, ServiceReference serviceRef) {
        Hashtable<String, Object> result = new Hashtable<String, Object>();
        result.put(KEY_ID, gadget.getId());
        for (String key : serviceRef.getPropertyKeys()) {
            result.put(key, serviceRef.getProperty(key));
        }
        return result;
    }
}
