# org.amdatu.opensocial

This project provides an OSGi-enabled version of an OpenSocial container based 
on Apache Shindig (2.5.0-beta-4). Aside wrapping Apache Shindig as OSGi bundle,
the project also provides means to use custom backends for storing OpenSocial-
related data, such as Persons, Messages and so on. In addition, a simple gadget 
provider is added, allowing easy registration, deregistration and listing of 
OpenSocial gadgets. This might be useful if you are building a dashboard where 
users can add or remove gadgets at runtime.


## Dependencies

The `org.amdatu.opensocial` project uses the following bundles at runtime:

* `osgi.cmpn`, version 4.2.1;
* `org.apache.felix.configadmin`, version 1.2.8 or later;
* `org.apache.felix.dependencymanager`, version 3.0 or later;
* `org.apache.felix.http.jetty`, version 2.2 or later;
* `org.apache.felix.http.whiteboard`, version 2.2 or later.


## Usage

In order to start and use the OpenSocial container, one *must* supply a valid
configuration using the service PID `org.amdatu.opensocial.shindig`. By default,
one can use the `shindig.properties` from the Shindig distribution for this.

### Custom backends

To implement custom backends for storing OpenSocial-related information, you 
should implement the accompanying `*BackendService` interface from the
`org.amdatu.opensocial.spi` package and register it using the name of that 
interface.

### Gadget provider

A default implementation of the gadget provider service is registered under 
`org.amdatu.opensocial.gadget.GadgetProvider` and allows you to retrieve the 
set of registered gadgets. A gadget is nothing more than an ID identifying the
gadget to the system and a set of URLs pointing to the actual OpenSocial gadget
specifications. 
The gadget provider only provides a single method: `getGadgets`, which returns
a set of gadgets. It accepts an optional (LDAP-style) filter clause to narrow
the returned set to a more specific set. This filter is applied to the service
properties of the registered gadgets, allowing you to filter gadgets on 
arbitrary properties.

The default implementation of the provider uses the whiteboard pattern to 
listen for all service (de)registrations of `org.amdatu.opensocial.gadget.Gadget`. 
This means that you can simply register your gadgets as OSGi-service and it 
will be picked up by the gadget provider automatically. For convenience, a 
default implementation of `org.amdatu.opensocial.gadget.Gadget` is provided in
the form of `org.amdatu.opensocial.gadget.SimpleGadget`. 


## Current issues

One known issue at the moment is that it is not possible to restart the 
`org.amdatu.opensocial` bundle very often. 


## License

Copyright (c) 2010-2012 The Amdatu Foundation

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
