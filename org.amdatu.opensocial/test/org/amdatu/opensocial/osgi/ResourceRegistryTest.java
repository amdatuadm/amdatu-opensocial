/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package org.amdatu.opensocial.osgi;

import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.net.URL;
import java.util.Dictionary;
import java.util.Properties;

import junit.framework.TestCase;

import org.amdatu.opensocial.Constants;
import org.mockito.invocation.InvocationOnMock;
import org.mockito.stubbing.Answer;
import org.osgi.framework.Bundle;

/**
 * Test cases for {@link ResourceRegistry}.
 */
public class ResourceRegistryTest extends TestCase {

    private ResourceRegistry m_registry;

    /**
     * Test method for {@link org.amdatu.opensocial.osgi.ResourceRegistry#getResource(java.lang.String)}.
     */
    public void testGetResource() {
        m_registry.registerResources(createMockBundle(1, "/path/to/resources/1"));
        m_registry.registerResources(createMockBundle(2, "/path/to"));
        m_registry.registerResources(createMockBundle(3, "/path/to/resources"));
        
        URL url;
        url = m_registry.getResource("/path/to/res");
        assertEquals("file://2/path/to/res", url);

        url = m_registry.getResource("/path/to/resources/test");
        assertEquals("file://3/path/to/resources/test", url);

        url = m_registry.getResource("/path/to/resources/1/test");
        assertEquals("file://1/path/to/resources/1/test", url);
    }

    /**
     * Tests that the registration of a bundle specifying a single resource works as expected.
     */
    public void testRegisterSingleBundleSingleResourceOk() {
        Bundle bundle = createMockBundle(1, "/resources");

        m_registry.registerResources(bundle);

        URL url = m_registry.getResource("/resources/foo");
        assertEquals("file://1/resources/foo", url);
    }

    /**
     * Tests that the registration of a bundle specifying multiple resources works as expected.
     */
    public void testRegisterSingleBundleMultipleResourcesOk() {
        Bundle bundle = createMockBundle(1, "/dir1", "/dir2", "/dir3");

        m_registry.registerResources(bundle);

        URL url;
        url = m_registry.getResource("/dir2/foo");
        assertEquals("file://1/dir2/foo", url);

        url = m_registry.getResource("/dir1/path/to/bar");
        assertEquals("file://1/dir1/path/to/bar", url);

        url = m_registry.getResource("/dir3");
        assertEquals("file://1/dir3", url);
    }

    /**
     * Tests that the registration of a bundle specifying multiple resources with prefixes works as expected.
     */
    public void testRegisterSingleBundleMultipleResourcesWithPrefixOk() {
        Bundle bundle = createMockBundle(1, "/dir1;prefix=bar", "/dir2;prefix=foo", "/dir3;prefix=", "/dir4;", "/dir5;qux");

        m_registry.registerResources(bundle);

        URL url;
        url = m_registry.getResource("/dir2/foo");
        assertEquals("file://1/foo/dir2/foo", url);

        url = m_registry.getResource("/dir1/path/to/bar");
        assertEquals("file://1/bar/dir1/path/to/bar", url);

        url = m_registry.getResource("/dir3");
        assertEquals("file://1/dir3", url);

        url = m_registry.getResource("/dir4");
        assertEquals("file://1/dir4", url);

        url = m_registry.getResource("/dir5");
        assertEquals("file://1/dir5", url);
    }

    /**
     * Tests that the registration of multiple bundles specifying multiple resources works as expected.
     */
    public void testRegisterMultipleBundleMultipleResourcesOk() {
        Bundle bundle1 = createMockBundle(1, "/dir1", "/dir2/bar", "/dir3");
        Bundle bundle2 = createMockBundle(2, "/dir2/foo", "/dir4", "/dir5");

        m_registry.registerResources(bundle1);
        m_registry.registerResources(bundle2);

        URL url;
        url = m_registry.getResource("/dir2/foo");
        assertEquals("file://2/dir2/foo", url);
        
        url = m_registry.getResource("/dir2/bar/test");
        assertEquals("file://1/dir2/bar/test", url);

        url = m_registry.getResource("/dir1/path/to/bar");
        assertEquals("file://1/dir1/path/to/bar", url);

        url = m_registry.getResource("/dir4");
        assertEquals("file://2/dir4", url);
    }

    /**
     * Tests that unregistering a bundle that formally has registered resources works as expected.
     */
    public void testUnregisterExistingResourceOk() {
        Bundle bundle1 = createMockBundle(1, "/dir1", "/dir2/bar", "/dir3");
        Bundle bundle2 = createMockBundle(2, "/dir2/foo", "/dir4", "/dir5");

        m_registry.registerResources(bundle1);
        m_registry.registerResources(bundle2);

        m_registry.unregisterResources(bundle2);

        // All resources of bundle2 should be removed...
        assertNull(m_registry.getResource("/dir2/foo/bar"));
        assertNull(m_registry.getResource("/dir4"));
        assertNull(m_registry.getResource("/dir5"));
        // All resources of bundle1 should remain as-is... 
        assertNotNull(m_registry.getResource("/dir1"));
        assertNotNull(m_registry.getResource("/dir2/bar/test"));
        assertNotNull(m_registry.getResource("/dir3"));
    }

    /**
     * Tests that unregistering a bundle that did not register resources works as expected.
     */
    public void testUnregisterNonExistingResourceOk() {
        Bundle bundle1 = createMockBundle(1, "/dir1", "/dir2/bar", "/dir3");
        Bundle bundle2 = createMockBundle(2, "/dir2/foo", "/dir4", "/dir5");

        m_registry.registerResources(bundle1);

        // bundle2 is never registered...
        m_registry.unregisterResources(bundle2);

        // All resources of bundle2 should be removed...
        assertNull(m_registry.getResource("/dir2/foo/bar"));
        assertNull(m_registry.getResource("/dir4"));
        assertNull(m_registry.getResource("/dir5"));
        // All resources of bundle1 should remain as-is... 
        assertNotNull(m_registry.getResource("/dir1"));
        assertNotNull(m_registry.getResource("/dir2/bar/test"));
        assertNotNull(m_registry.getResource("/dir3"));
    }

    @Override
    protected void setUp() throws Exception {
        m_registry = new ResourceRegistry();
    }
    
    private void assertEquals(String expected, URL url) {
        assertNotNull("URL was null, should be: " + expected, url);
        assertEquals(expected, url.toExternalForm());
    }

    /**
     * Creates a mock bundle with the specified resources in its bundle header.
     * 
     * @param resources the resources to specify in the bundle header.
     * @return a mock bundle instance, never <code>null</code>.
     */
    private Bundle createMockBundle(final int id, String... resources) {
        StringBuilder sb = new StringBuilder();
        for (String resource : resources) {
            if (sb.length() > 0) {
                sb.append(", ");
            }
            sb.append(resource);
        }

        Dictionary headers = new Properties();
        headers.put(Constants.RESOURCE_PROVIDER_KEY, sb.toString());

        Bundle mock = mock(Bundle.class);
        when(mock.getHeaders()).thenReturn(headers);
        when(mock.getResource(anyString())).thenAnswer(new Answer<URL>() {
            @Override
            public URL answer(InvocationOnMock invocation) throws Throwable {
                String input = (String) invocation.getArguments()[0];
                return new URL("file://" + id + "/" + input);
            }
        });
        return mock;
    }
}
