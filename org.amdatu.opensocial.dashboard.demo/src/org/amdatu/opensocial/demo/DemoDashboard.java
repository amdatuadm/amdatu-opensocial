/*
 * Copyright (c) 2010-2012 - The Amdatu Foundation.
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *      http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package org.amdatu.opensocial.demo;

import org.apache.felix.dm.Component;
import org.osgi.service.http.HttpContext;
import org.osgi.service.http.HttpService;

/**
 * Registers the demo dashboard content.
 */
public class DemoDashboard {

    private volatile HttpService m_service;
    private volatile HttpContext m_context;

    /**
     * Called by Felix DM.
     */
    public void start(Component component) throws Exception {
        m_service.registerResources("/", "/", m_context);
    }

    /**
     * Called by Felix DM.
     */
    public void stop(Component component) throws Exception {
        m_service.unregister("/");
    }
}
