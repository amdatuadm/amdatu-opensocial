/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 *   http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
package org.amdatu.opensocial.demo;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;

import org.amdatu.opensocial.Constants;
import org.osgi.service.cm.Configuration;
import org.osgi.service.cm.ConfigurationAdmin;
import org.osgi.service.log.LogService;

/**
 * Provides a proper configuration for this demo dashboard.
 */
public class DemoConfiguration {
    private volatile ConfigurationAdmin m_configAdmin;
    private volatile LogService m_logService;

    public void start() {
        try {
            Configuration configuration = m_configAdmin.getConfiguration(Constants.SHINDIG_CONFIGURATION_PID, null);
            Properties properties = new Properties();

            InputStream is = getClass().getResourceAsStream("/resources/shindig.properties");
            if (is != null) {
                properties.load(is);
            }
            else {
                m_logService.log(LogService.LOG_WARNING, "Failed to locate & load properties file!");
            }
            configuration.update(properties);
        }
        catch (IOException e) {
            e.printStackTrace();
        }
    }
}
