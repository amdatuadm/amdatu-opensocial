# org.amdatu.opensocial.dashboard.demo

This project provides a dashboard demonstrating some of the features provided 
by `org.amdatu.opensocial`.


## Dependencies

The `org.amdatu.opensocial.dashboard.demo` project uses the following bundles 
at runtime:

* `osgi.cmpn`, version 4.2.1;
* `org.amdatu.opensocial`, version 1.0.7 or later;
* `org.apache.felix.configadmin`, version 1.2.8 or later;
* `org.apache.felix.dependencymanager`, version 3.0 or later;
* `org.apache.felix.http.jetty`, version 2.2 or later;
* `org.apache.felix.http.whiteboard`, version 2.2 or later.


## Usage

In order to start the dashboard, one can either use the `run-demo.bndrun` run
configuration for Bndtools, or add all required bundles (see Dependencies) to
an existing OSGi container (note that the startup of the dashboard takes a 
little while). 

The dashboard can be accessed at <http://localhost:8080/index.html>. By default,
the dashboard shows a simple dropdown list from which you can choose gadgets,
which can be added to the dashboard with the `Add` button.


## Current issues

The current dashboard implementation is relatively simple and does not show
all the nice features of OpenSocial directly.


## License

Copyright (c) 2010-2012 The Amdatu Foundation

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

      http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
